name := "trustmarket"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  javaJdbc,
  javaEbean,
  cache,
  "org.postgresql" % "postgresql" % "9.3-1100-jdbc4",
  "com.typesafe" %% "play-plugins-mailer" % "2.2.0",
  "org.mindrot" % "jbcrypt" % "0.3m",
  "pdf" % "pdf_2.10" % "0.5" exclude("org.scala-stm", "scala-stm_2.10.0") exclude("play", "play_2.10"),
  "com.lowagie" % "itext"  % "2.0.8",
  "org.apache.directory.studio" % "org.apache.commons.io" % "2.4"
)     

play.Project.playJavaSettings
