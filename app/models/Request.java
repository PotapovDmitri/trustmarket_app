package models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import utils.DateUtils;
import utils.FilesUtils;
import utils.MoneyUtils;
import forms.RequestForm;

/**
 * User entity managed by Ebean
 */
@Entity
@Table(name = "request")
public class Request extends Model implements Serializable {

	private static final long serialVersionUID = 1L;

	public enum RequestType {
		FULL, DELIVERY;
	}

	@Id
	@GeneratedValue
	@Column(name = "request_id")
	private Long requestId;

	@Constraints.Required
	private String type;

	@Column(name = "store_link")
	private String storeLink;

	private String name;

	private String size;

	private String article;
	
	private String packageCode;

	private Integer price;

	private Integer totalSumma;

	private Integer count;

	private String color;

	private String description;

	@Formats.DateTime(pattern = "dd.MM.yyyy")
	private Date created;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "user_request_id")
	private UserRequests userRequests;

	// -- Queries
	public static Model.Finder<Long, Request> find = new Model.Finder<Long, Request>(
			Long.class, Request.class);

	public Request(String storeLink, String name, String size, String article,
			Double price, Integer count, String color, String description,
			RequestType type, UserRequests userRequests, User user) {
		
		this.type = type.toString();
		this.storeLink = storeLink;
		this.name = name;
		this.size = size;
		this.article = article;
		this.totalSumma = MoneyUtils.convertToCents(price) * count;
		this.count = count;
		this.color = color;
		this.price = MoneyUtils.convertToCents(price);
		this.description = description;
		this.created = DateUtils.parseDate(new Date());
		this.userRequests = userRequests;
	}
	
	public Request(Double price, String packageCode, String description,
			RequestType type, UserRequests userRequests, User user) {
		
		this.type = type.toString();
		this.packageCode = packageCode;
		this.price = MoneyUtils.convertToCents(price);
		this.totalSumma = MoneyUtils.convertToCents(price);
		this.count = 1;
		this.description = description;
		this.created = DateUtils.parseDate(new Date());
		this.userRequests = userRequests;
	}


	public static Request createRequest(RequestForm form,
			UserRequests userRequests, User user) {
		Request reqest = null;
		
		if("1".equals(form.getType())){
			reqest = new Request(form.storelink, form.name, form.size,
					form.article, form.price, form.count, form.color,
					form.description, RequestType.FULL, userRequests, user);
		}else{
			reqest = new Request(form.price, form.packagecode, form.description, RequestType.DELIVERY, userRequests, user);
		}
		reqest.save();
		return reqest;
	}

	public static Request updateRequest(RequestForm form) {
		Request request = findById(form.getRequestId());

		if("1".equals(form.getType())){
			request.setName(form.getName());
			request.setStoreLink(form.getStorelink());
			request.setColor(form.getColor());
			request.setCount(form.getCount());
			request.setSize(form.getSize());
			request.setArticle(form.getArticle());

			request.setTotalSumma(MoneyUtils.convertToCents(form.getPrice()) * form.getCount());
	
		}else{
			request.setPackageCode(form.getPackagecode());
			request.setTotalSumma(MoneyUtils.convertToCents(form.getPrice()));
		}
		request.setPrice(MoneyUtils.convertToCents(form.getPrice()));
		request.setDescription(form.getDescription());

		request.update();
		return request;
	}

	public static void deleteRequest(Long id) {
		find.ref(id).delete();
	}

	public static Request findById(Long id) {
		return find.fetch("userRequests").where().eq("requestId", id)
				.findUnique();
	}

	public static List<Request> findRequestsByUser(Long userId) {
		return find.fetch("userRequests").where().eq("user.userId", userId)
				.findList();
	}

    
    public List<String> getDocuments(){
    	return FilesUtils.getFilesCode(getRequestId(), userRequests.getUser().getUserId());
    }
    
	/**
	 * Retrieve all users.
	 */
	public static List<Request> findAll() {
		return find.all();
	}

	public Long getRequestId() {
		return requestId;
	}

	public void setRequestId(Long requestId) {
		this.requestId = requestId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getArticle() {
		return article;
	}

	public void setArticle(String article) {
		this.article = article;
	}

	public Integer getTotalSumma() {
		return totalSumma;
	}

	public void setTotalSumma(Integer totalSumma) {
		this.totalSumma = totalSumma;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public String getStoreLink() {
		return storeLink;
	}

	public void setStoreLink(String storeLink) {
		this.storeLink = storeLink;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public UserRequests getUserRequests() {
		return userRequests;
	}

	public void setUserRequests(UserRequests userRequests) {
		this.userRequests = userRequests;
	}

	public String getPackageCode() {
		return packageCode;
	}

	public void setPackageCode(String packageCode) {
		this.packageCode = packageCode;
	}
}