package models;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import play.data.format.Formats;
import play.db.ebean.Model;
import utils.DateUtils;
import forms.MessageForm;

@Entity  
@Table(name="request_messages")  
public class RequestMessages extends Model {
	private static final long serialVersionUID = 1L;
	
    public enum MessageType {
        QUESTION, QUESTION_ANSWER;
    }
    
	@Id  
    @GeneratedValue  
    @Column(name="request_message_id")
    private Long requestMessageId;  

	private String message;
	
    @Column(name="request_message_type")
	private String messageType;
	
    @ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)  
    @JoinColumn(name="user_request_id")
    private UserRequests userRequests;  

    @ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)  
    @JoinColumn(name="user_id")
	private User user;
    
    @ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)  
    @JoinColumn(name="reply_message_id")
    private RequestMessages replyMessage;
    
    @OneToMany(cascade=CascadeType.ALL, mappedBy="replyMessage", fetch=FetchType.LAZY)
    private List<RequestMessages> replyMessages;
	
    @Formats.DateTime(pattern = "dd.MM.yyyy")
	private Date created;
	
	public static Model.Finder<String,RequestMessages> find = new Model.Finder<String,RequestMessages>(String.class, RequestMessages.class);
    
	public RequestMessages(String message, Long userRequestId, MessageType type, User user){
		this.message = message;
		this.userRequests = UserRequests.findById(userRequestId);
		this.user = user;
		this.messageType = type.toString();
		this.created = DateUtils.parseDate(new Date());
	}
	
	public RequestMessages(String message, Long replyMessageId, Long userRequestId, MessageType type, User user){
		this.message = message;
		this.replyMessage = RequestMessages.findById(replyMessageId);
		this.userRequests = UserRequests.findById(userRequestId);
		this.user = user;
		this.messageType = type.toString();
		this.created = DateUtils.parseDate(new Date());
	}


    public static RequestMessages findById(Long id) {
        return find.where().eq("request_message_id", id).findUnique();
    } 
    
    
    public static RequestMessages createMessage(MessageForm form, User user) {
    	RequestMessages message = new RequestMessages(form.message, form.origmsgid, form.userrequestid, (form.getOrigmsgid() != null && form.getOrigmsgid() > 0 ? MessageType.QUESTION_ANSWER: MessageType.QUESTION), user);
    	message.save();
        return message;
    }
    
    public static RequestMessages applyReplyMessage(Long messageId, RequestMessages replyMessage) {
    	RequestMessages message = findById(messageId);
    	message.setReplyMessage(replyMessage);
    	message.update();
        return message;
    }
    
    
	public Long getRequestMessageId() {
		return requestMessageId;
	}

	public void setRequestMessageId(Long requestMessageId) {
		this.requestMessageId = requestMessageId;
	}

	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}

	public UserRequests getUserRequests() {
		return userRequests;
	}

	public void setUserRequests(UserRequests userRequests) {
		this.userRequests = userRequests;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public RequestMessages getReplyMessage() {
		return replyMessage;
	}

	public void setReplyMessage(RequestMessages replyMessage) {
		this.replyMessage = replyMessage;
	}

	public List<RequestMessages> getReplyMessages() {
		return replyMessages;
	}

	public void setReplyMessages(List<RequestMessages> replyMessages) {
		this.replyMessages = replyMessages;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	
}
