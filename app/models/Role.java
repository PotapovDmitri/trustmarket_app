package models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import play.db.ebean.Model;

@Entity  
@Table(name="roles")  
public class Role extends Model {

    private static final long serialVersionUID = 1L;
    
    public enum RoleName {
        ADMIN, USER;
    }
    
	@Id  
    @GeneratedValue  
    @Column(name="role_id")
    private Long roleId;  
	
	@Column(name = "role_name")
    private String role;  

	@OneToMany(cascade=CascadeType.ALL, mappedBy = "role")
    private List<User> userList;  

	
    // -- Queries
    public static Model.Finder<String,Role> find = new Model.Finder<String,Role>(String.class, Role.class);
    
    /**
     * Retrieve a User from email.
     */
    public static Role findByName(String name) {
        return find.where().eq("role_name", name).findUnique();
    } 
    public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	public String getRole() {  
        return role;  
    }  
	public void setRole(String role) {  
        this.role = role;  
    }  
    public List<User> getUserList() {  
        return userList;  
    }  
    public void setUserList(List<User> userList) {  
        this.userList = userList;  
    }  
}
