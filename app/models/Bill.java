package models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import play.data.format.Formats;
import play.db.ebean.Model;
import utils.DateUtils;
import utils.MoneyUtils;
import forms.BillForm;

/**
 * User entity managed by Ebean
 */
@Entity 
@Table(name="bills")
public class Bill extends Model implements Serializable {

    private static final long serialVersionUID = 1L;
    
    public enum BillStates {
        CREATED, PAYED, UNPAYED, CANCELED;
    }
    
    @Id  
    @GeneratedValue
    @Column(name="bill_id")
    private Long billId;  
      
    private Integer totalSumma;
    
    private Integer productSumma;
        
    private Integer serviceSumma;
    
    private Integer deliverySumma;
    
    @Formats.DateTime(pattern = "dd.MM.yyyy")
    private Date created;
    
    @Formats.DateTime(pattern = "dd.MM.yyyy")
    private Date actionDate;
    
    @Formats.DateTime(pattern = "dd.MM.yyyy")
    private Date payed;

    @Formats.DateTime(pattern = "dd.MM.yyyy")
    private Date unpayed;

    private String billState;
    
    @OneToOne(fetch=FetchType.LAZY)  
    @JoinColumn(name="user_request_id")    
    private UserRequests userRequests;
    
    @Column(name="payment_method")
    private String paymentMehtod;
    
    // -- Queries
    public static Model.Finder<String,Bill> find = new Model.Finder<String,Bill>(String.class, Bill.class);
    
    public Bill(Double totalSumma, Double productSumma, Double deliverySumma, Double serviceSumma, Long userRrequestId, BillStates state){
    	this.totalSumma = MoneyUtils.convertToCents(totalSumma);
    	this.productSumma = MoneyUtils.convertToCents(productSumma);
    	this.deliverySumma = MoneyUtils.convertToCents(deliverySumma);
    	this.serviceSumma = MoneyUtils.convertToCents(serviceSumma);
		this.userRequests = UserRequests.findById(userRrequestId);
		this.billState = state.toString();
		this.created = DateUtils.parseDate(new Date());
    }
    

    public static Bill findById(Long id) {
        return find.where().eq("bill_id", id).findUnique();
    } 
    
    public static Bill createBill(BillForm form) {
    	Bill bill = new Bill(form.totalsumma, form.productsumma,form.deliverysumma, form.servicesumma, form.userrequestid, BillStates.CREATED);
    	bill.save();
        return bill;
    }
    public static Bill updateBillState(Long billId, BillStates state) {
    	Bill bill = findById(billId);
    	bill.setBillState(state.toString());
    	if(BillStates.PAYED.equals(state)){
    		bill.setPayed(DateUtils.parseDate(new Date()));
    	}else if(BillStates.UNPAYED.equals(state)){
    		bill.setUnpayed(DateUtils.parseDate(new Date()));
    	}
    	bill.setActionDate(DateUtils.parseDate(new Date()));
    	bill.update();
        return bill;
    }

    /**
     * Retrieve all users.
     */
    public static List<Bill> findAll() {
        return find.all();
    }

  
	public Long getBillId() {
		return billId;
	}


	public void setBillId(Long billId) {
		this.billId = billId;
	}

	public Integer getTotalSumma() {
		return totalSumma;
	}

	public void setTotalSumma(Integer totalSumma) {
		this.totalSumma = totalSumma;
	}

	public Integer getProductSumma() {
		return productSumma;
	}
	public void setProductSumma(Integer productSumma) {
		this.productSumma = productSumma;
	}


	public Integer getServiceSumma() {
		return serviceSumma;
	}

	public void setServiceSumma(Integer serviceSumma) {
		this.serviceSumma = serviceSumma;
	}

	public Integer getDeliverySumma() {
		return deliverySumma;
	}

	public void setDeliverySumma(Integer deliverySumma) {
		this.deliverySumma = deliverySumma;
	}

	public UserRequests getUserRequests() {
		return userRequests;
	}


	public void setUserRequests(UserRequests userRequests) {
		this.userRequests = userRequests;
	}


	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	
	public Date getActionDate() {
		return actionDate;
	}


	public void setActionDate(Date actionDate) {
		this.actionDate = actionDate;
	}


	public String getBillState() {
		return billState;
	}

	public void setBillState(String billState) {
		this.billState = billState;
	}


	public Date getPayed() {
		return payed;
	}
	public void setPayed(Date payed) {
		this.payed = payed;
	}

	public Date getUnpayed() {
		return unpayed;
	}
	public void setUnpayed(Date unpayed) {
		this.unpayed = unpayed;
	}

	public String getPaymentMehtod() {
		return paymentMehtod;
	}

	public void setPaymentMehtod(String paymentMehtod) {
		this.paymentMehtod = paymentMehtod;
	}
}