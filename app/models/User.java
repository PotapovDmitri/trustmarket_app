package models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import models.Role.RoleName;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import services.PasswordService;
import utils.DateUtils;
import forms.RegistrationForm;
import forms.UserForm;

/**
 * User entity managed by Ebean
 */
@Entity 
@Table(name="users")
public class User extends Model implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id  
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="user_id")
    private Long userId;  
      
    @Constraints.Required
    @Formats.NonEmpty
    private String username;  
      
    @Constraints.Required
    @Formats.NonEmpty
    @Column(name = "password", length = 255, columnDefinition = "VARCHAR(255)")
    private String password;  
    
    private String firstName;
    
    private String lastName;
    
    @Formats.DateTime(pattern = "dd.MM.yyyy")
    private Date birthday;
    
    private String passportNr;
    
    @Constraints.Required
    @Formats.NonEmpty
    private String email;
    
    @Formats.DateTime(pattern = "dd.MM.yyyy")
    private Date created;

    @ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)  
    @JoinColumn(name="role_id")
    private Role role;
    
	@OneToMany(cascade=CascadeType.ALL, mappedBy = "user")
    private List<UserRequests> userRequests;  
	

    // -- Queries
    public static Model.Finder<String,User> find = new Model.Finder<String,User>(String.class, User.class);
    
    public User(){ }
    
    public User(String username, String password, String firstName, String lastName, Date birthday, String passportNr, String email, RoleName role) {
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
        this.passportNr = passportNr;
        this.email = email;
        this.role = Role.findByName(role.toString());
        this.created = DateUtils.parseDate(new Date());
    }
    
    /**
     * Retrieve all users.
     */
    public static List<User> findAll() {
        return find.all();
    }

    /**
     * Retrieve a User from email.
     */
    public static User findByUsername(String username) {
        return find.fetch("role").fetch("userRequests").fetch("userRequests.request").where().eq("username", username).findUnique();
    }

    public static User findByEmail(String email) {
        return find.fetch("role").fetch("userRequests").fetch("userRequests.request").where().eq("email", email).findUnique();
    }
    public static User findById(Integer userId) {
        return find.fetch("role").fetch("userRequests").fetch("userRequests.request").where().eq("userId", userId).findUnique();
    }

    public static List<User> findUsers() {
        return find.fetch("role").fetch("userRequests").fetch("userRequests.request").where().eq("role.role", "USER").findList();
    }
    /**
     * Authenticate a User.
     */
    public static User authenticate(String username, String password) {
        return find.fetch("role").fetch("userRequests").fetch("userRequests.request").where()
            .eq("username", username)
            .eq("password", password)
            .findUnique();
    }
    
    public static User updateUser(UserForm form){
    	User user = findById(form.getUserid());
    	
    	user.setFirstName(form.getFirstname());
    	user.setLastName(form.getLastname());
    	user.setBirthday(DateUtils.parseDate(form.getBirthdate()));
    	user.setPassportNr(form.getPassportnr());
    	user.setEmail(form.getEmail());
    	
    	user.update();

    	return user;
    }
    
    /**
     * Create a new project.
     */
    public static User createUser(RegistrationForm form) {
    	User user = new User(form.username, PasswordService.createPassword(form.password), form.firstname, form.lastname, form.birthdate, form.passportnr, form.email, RoleName.USER);
    	user.save();
        return user;
    }
    
    public String toString() {
        return "User(" + username + ")";
    }

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
    
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getPassportNr() {
		return passportNr;
	}

	public void setPassportNr(String passportNr) {
		this.passportNr = passportNr;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public List<UserRequests> getUserRequests() {
		return userRequests;
	}

	public void setUserRequests(List<UserRequests> userRequests) {
		this.userRequests = userRequests;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
}