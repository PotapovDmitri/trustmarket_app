package models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import models.Bill.BillStates;
import models.RequestMessages.MessageType;
import play.data.format.Formats;
import play.db.ebean.Model;
import utils.DateUtils;
import utils.MoneyUtils;
import utils.StringUtils;

import com.avaje.ebean.Expr;

@Entity 
@Table(name="user_requests")
public class UserRequests extends Model implements Serializable {

    private static final long serialVersionUID = 1L;

    public enum UserRequestStates {
        CREATED, SENDED;
    }
    
    @Id  
    @GeneratedValue
    @Column(name="user_request_id")
    private Long userRequestId;  

    @Column(name="delivery_price")
    private Integer deliveryPrice;
    
    private String status;
    
    @Formats.DateTime(pattern = "dd.MM.yyyy")
    private Date created;

    @Formats.DateTime(pattern = "dd.MM.yyyy")
    private Date sended;

    @ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)  
    @JoinColumn(name="user_id")
    private User user;  

    @OneToOne(mappedBy = "userRequests", fetch=FetchType.LAZY)
    private Bill bill;  

	@OneToMany(cascade=CascadeType.ALL, mappedBy = "userRequests", fetch=FetchType.LAZY)
    private List<Request> request;  

	@OneToMany(cascade=CascadeType.ALL, mappedBy = "userRequests", fetch=FetchType.LAZY)
    private List<RequestMessages> messages;  
	
	  /**
     * Retrieve all users.
     */
    public static List<UserRequests> findAll() {
        return find.all();
    }
    
    public UserRequests(User user){ 
    	this.user = user;
    	this.status = UserRequestStates.CREATED.toString();
		this.created = DateUtils.parseDate(new Date());

    }

    // -- Queries
    public static Model.Finder<Long, UserRequests> find = new Model.Finder<Long,UserRequests>(Long.class, UserRequests.class);

    public static void delete(Long id){
    	find.ref(id).delete();
    }
    
    public static List<UserRequests> findAllSended() {
        return find.fetch("request")
        		.fetch("messages")
        		.fetch("bill")
        		.fetch("messages.replyMessages")
        		.where()
        		.eq("status", UserRequestStates.SENDED.toString()).findList();
    } 
    
    public static UserRequests findById(Long id) {
        return find.fetch("request").fetch("messages").fetch("bill").fetch("messages.replyMessages").where().eq("userRequestId", id).findUnique();
    } 
    
    public static List<UserRequests> findUserCreatedRequests(Long userId) {
        return find.fetch("request")
        		.fetch("messages")
        		.fetch("bill")
        		.fetch("messages.replyMessages")
        		.where()
        		.eq("user.userId", userId)
        		.or(Expr.isNull("bill"), Expr.eq("bill.billState", BillStates.CREATED.toString()))
        		.orderBy("created desc")
        		.findList();
    } 
    public static List<UserRequests> findUsersCreatedRequests() {
        return find.fetch("request")
        		.fetch("messages")
        		.fetch("bill")
        		.fetch("messages.replyMessages")
        		.where()
        		.eq("status", UserRequestStates.SENDED.toString())
        		.or(Expr.isNull("bill"), Expr.eq("bill.billState", BillStates.CREATED.toString()))
        		.findList();
    } 
    public static List<UserRequests> findUserNotPayedRequests(Long userId) {
        return find.fetch("request")
        		.fetch("messages")
        		.fetch("bill")
        		.fetch("messages.replyMessages")
        		.where()
        		.eq("user.userId", userId)
        		.or(Expr.isNull("bill"), Expr.ne("bill.billState", BillStates.PAYED.toString()))
        		.findList();
    } 
    public static List<UserRequests> findUserPayedRequests(Long userId) {
        return find.fetch("request")
        		.fetch("messages")
        		.fetch("bill")
        		.fetch("messages.replyMessages")
        		.where()
        		.eq("user.userId", userId)
        		.eq("bill.billState", BillStates.PAYED.toString())
        		.findList();
    }
    
    public static List<UserRequests> findUserHistoryRequests(Long userId) {
        return find.fetch("request")
        		.fetch("messages")
        		.fetch("bill")
        		.fetch("messages.replyMessages")
        		.where()
        		.eq("user.userId", userId)
        		.ne("bill.billState", BillStates.CREATED.toString())
        		.findList();
    } 
    public static List<UserRequests> findUsersHistoryRequests() {
        return find.fetch("request")
        		.fetch("messages")
        		.fetch("bill")
        		.fetch("messages.replyMessages")
        		.where()
        		.ne("bill.billState", BillStates.CREATED.toString())
        		.findList();
    } 
    public static UserRequests updateDeliveryPrice(Long id, Double deliveryPrice) {
    	UserRequests request = findById(id);
    	request.setDeliveryPrice(MoneyUtils.convertToCents(deliveryPrice));
    	request.update();
    	
    	return request;
    }
    public static UserRequests sendrequest(Long id) {
    	UserRequests request = findById(id);
    	request.setStatus(UserRequestStates.SENDED.toString());
    	request.setSended(DateUtils.parseDate(new Date()));

    	request.update();
    	
    	return request;
    }

    public static UserRequests createUserRequest(User user) {
    	UserRequests reqest = new UserRequests(user);
    	reqest.save();
        return reqest;
    }
    
    public static UserRequests createUserRequestOfPossibleReq(User user, Double deliveryPrice) {
    	UserRequests request = new UserRequests(user);
    	request.setDeliveryPrice(MoneyUtils.convertToCents(deliveryPrice));
    	request.save();
        return request;
    }
    
    
    public Integer getTotalRequestsTotalPrices(){
		Integer prices = 0; 
    	if(request.size() > 0){
    		for(Request request: this.request){
    			prices = prices + (request.getTotalSumma() != null ? request.getTotalSumma() : 0);
    		}
    	}
    	//return NumericUtils.round(prices, 2);
    	return prices;
    }
    
    public Integer getCommissionPrice(){
    	Integer requestsprices = 0; 
    	if(request.size() > 0){
    		for(Request request: this.request){
    			requestsprices = requestsprices + (request.getTotalSumma() != null ? request.getTotalSumma() : 0);
    		}
    	}
    	
    	//Double commission10 = requestsprices * 0.1;
    	int commission10 = (int)(requestsprices*(10/100.0f));
    	if(1000 > commission10){
    		return 1000;
    	}
    	
    	return commission10;
    }
    
    public String getRequestsType(){
    	if(request.size() > 0){
    		return this.request.get(0).getType();
    	}
    	
    	return null;
    }
    public Integer getTotalPrice(){
    	//return NumericUtils.round(getTotalRequestsTotalPrices() +  getCommissionPrice() + getDeliveryPrice(), 2);
    	String type = getRequestsType();
    	return (type != null && "FULL".equals(type) ? (getTotalRequestsTotalPrices() + getDeliveryPrice()) : 0 ) +  getCommissionPrice();
    }
  
	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}


	public Bill getBill() {
		return bill;
	}


	public void setBill(Bill bill) {
		this.bill = bill;
	}

	public Long getUserRequestId() {
		return userRequestId;
	}

	public void setUserRequestId(Long userRequestId) {
		this.userRequestId = userRequestId;
	}

	public List<Request> getRequest() {
		return request;
	}

	public void setRequest(List<Request> request) {
		this.request = request;
	}
	public List<RequestMessages> getMessages() {
		return messages;
	}
	public void setMessages(List<RequestMessages> messages) {
		this.messages = messages;
	}
	public List<RequestMessages> getQuestionMessages() {
		List<RequestMessages> msgs = new ArrayList<RequestMessages>();
		for(RequestMessages msg : messages){
			if(MessageType.QUESTION.toString().equals(msg.getMessageType())){
				msgs.add(msg);
			}
		}
		return msgs;
	}

	public Integer getDeliveryPrice() {
		return deliveryPrice != null ? deliveryPrice : 0;
	}
	public void setDeliveryPrice(Integer deliveryPrice) {
		this.deliveryPrice = deliveryPrice;
	}

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getSended() {
		return sended;
	}

	public void setSended(Date sended) {
		this.sended = sended;
	}
}