package models;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import play.db.ebean.Model;

@Entity
@Table(name = "admin_stores")
public class AdminStores extends Model implements Serializable {

	private static final long serialVersionUID = 1L;

	public enum StoreView {
		NONE, SCROLL_LIST, MODAL;
	}

	@Id
	@GeneratedValue
	@Column(name = "store_id")
	private Long storeId;

	@Column(name = "name_ru")
	private String nameRu;

	@Column(name = "name_est")
	private String nameEst;

	@Column(name = "name_eng")
	private String nameEng;

	@Column(name = "desc_ru")
	private String descRu;

	@Column(name = "desc_est")
	private String descEst;

	@Column(name = "desc_eng")
	private String descEng;

	private String image;

	private String link;

	@Column(name = "store_view")
	private boolean storeView;

	// -- Queries
	public static Model.Finder<Long, AdminStores> find = new Model.Finder<Long, AdminStores>(
			Long.class, AdminStores.class);

	/**
	 * Retrieve all users.
	 */
	public static List<AdminStores> findAll() {
		return find.all();
	}

	public Long getStoreId() {
		return storeId;
	}

	public void setStoreId(Long storeId) {
		this.storeId = storeId;
	}

	public String getNameRu() {
		return nameRu;
	}

	public void setNameRu(String nameRu) {
		this.nameRu = nameRu;
	}

	public String getNameEst() {
		return nameEst;
	}

	public void setNameEst(String nameEst) {
		this.nameEst = nameEst;
	}

	public String getNameEng() {
		return nameEng;
	}

	public void setNameEng(String nameEng) {
		this.nameEng = nameEng;
	}

	public String getDescRu() {
		return descRu;
	}

	public void setDescRu(String descRu) {
		this.descRu = descRu;
	}

	public String getDescEst() {
		return descEst;
	}

	public void setDescEst(String descEst) {
		this.descEst = descEst;
	}

	public String getDescEng() {
		return descEng;
	}

	public void setDescEng(String descEng) {
		this.descEng = descEng;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public boolean isStoreView() {
		return storeView;
	}

	public void setStoreView(boolean storeView) {
		this.storeView = storeView;
	}
}
