package utils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class MoneyUtils {

	public static int parsePrice(String value, double onFailure) {
		double doublPrice = NumericUtils.tryToParseDouble(value, onFailure);
		return (int) Math.round(doublPrice * 100.0);
	}

	public static String priceToString(Double price){
		DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
	    decimalFormatSymbols.setDecimalSeparator('.');
	    decimalFormatSymbols.setGroupingSeparator(' ');
	    DecimalFormat decimalFormat = new DecimalFormat("#,##0.00", decimalFormatSymbols);
	    return decimalFormat.format(price);
	}

	public static String priceToString(Integer price){
		DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
	    decimalFormatSymbols.setDecimalSeparator('.');
	    decimalFormatSymbols.setGroupingSeparator(' ');
	    DecimalFormat decimalFormat = new DecimalFormat("#,##0.00", decimalFormatSymbols);
	    return decimalFormat.format(price);
	}

	public static Integer convertToCents(Double price){
		if(price == null) return 0;
		return (int)(price * 100);
	}
	
	public static String asMoney(double money) {
        DecimalFormat df = new DecimalFormat("#,##0.00");
        return df.format(money);
    }

    public static String asDecimal(double decimal) {
        DecimalFormat df = new DecimalFormat("#,##0.##");
        return df.format(decimal);
    }
    
    public static String asMoneyB(double money) {
        DecimalFormat df = new DecimalFormat("#,##0.00 DB;#,##0.00 CR");
        return df.format(money);
    }

    public static String asQuantity(double quantity) {
        DecimalFormat df = new DecimalFormat("#,##0.##");
        return df.format(quantity);
    }
	/*
	public static String formatPrice(String cents, String locale) {
		return formatPrice(NumericUtils.tryToParseInt(cents, 0), locale, null);
	}

	
	public static String formatPrice(int cents, String locale) {
		return formatPrice(cents, locale, null);
	}

	public static String formatPrice(String cents, String locale,
			String currency) {
		return formatPrice(NumericUtils.tryToParseInt(cents, 0), locale,
				currency);
	}
	*/
    public static String formatPrice(int cents) {
    	return formatPrice(cents, null);
    }
    
    public static String formatPrice(int cents, String currency) {

	    boolean inFrontOfAmount = false;

	    // Currency in front of amount
	    if ("$".equals(currency) ||
	        "£".equals(currency)||
	        "R$".equals(currency))
	      inFrontOfAmount = true;

	    
	    
	    DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
	    decimalFormatSymbols.setDecimalSeparator('.');
	    decimalFormatSymbols.setGroupingSeparator(' ');
	    DecimalFormat df = new DecimalFormat("#,##0.00", decimalFormatSymbols);
	    
	    df.setDecimalSeparatorAlwaysShown(true);
	    
	   /* 
	    DecimalFormat df = new DecimalFormat();

	    // Number from 5 to 5.
	    df.setDecimalSeparatorAlwaysShown(true);

	    DecimalFormatSymbols dfs = new DecimalFormatSymbols();
	    dfs.setGroupingSeparator(' ');
	    df.setDecimalFormatSymbols(dfs);*/

	    String amount = df.format(cents / 100.0);
	    char[] arr = amount.toCharArray();

	    // Number from 5. to 5.00
	    if (Character.isDigit(arr[arr.length - 1]) == false) amount = amount + "00";
	    // Number from 5.5 to 5.50
	    if (arr.length > 1 &&
	        Character.isDigit(arr[arr.length - 2]) == false) amount = amount + "0";

	    // No currency, remove also white spaces
	    if (currency == null) {
	      amount = amount.replaceAll("[ ]", "");
	      return amount;
	    }

	    return (inFrontOfAmount ? currency : "") + amount + (inFrontOfAmount ? "" : (" " + currency));
	  }

	/*
	  public static String formatVAT(String vat, String locale) {
	    return formatVAT(NumericUtils.tryToParseInt(vat, 0), locale, "%");
	  }

	  public static String formatVATWithoutPercentage(String vat, String locale) {
	    return formatVAT(NumericUtils.tryToParseInt(vat, 0), locale, "");
	  }

	  public static String formatVAT(int vat, String locale) {
	    return formatVAT(vat, locale, "%");
	  }

	  public static String formatVATWithoutPercentage(int vat, String locale) {
	    return formatVAT(vat, locale, "");
	  }


	  public static String formatVAT(int vat, String locale, String percentage) {


	    DecimalFormat df = (DecimalFormat)NumberFormat.getInstance(Locales.getLocaleClass(locale));

	    // Number from 5 to 5.
	    df.setDecimalSeparatorAlwaysShown(false);
	    String amount = df.format(vat / 100.0);
	    return amount + percentage;
	  }*/
	

}
