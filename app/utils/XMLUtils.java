package utils;

import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import play.Play;

public class XMLUtils {

	public static String CONDITIONS_FILE = "conditional.xml";
	
	public static String getCondition(String lang){
		
		String content = "";
		
		// create a new DocumentBuilderFactory
	    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	    try {
			// use the factory to create a documentbuilder
	        DocumentBuilder builder = factory.newDocumentBuilder();
	        
			InputStream is = Play.application().resourceAsStream(CONDITIONS_FILE);
			
	        Document doc = builder.parse(is);
	
	        // get the first element
	        Element element = doc.getDocumentElement();
	
	        // get all child nodes
	        NodeList nodes = element.getChildNodes();
	
	        // print the text content of each child
	        for (int i = 0; i < nodes.getLength(); i++) {
	        	if(nodes.item(i).getNodeName().equals("condition")){
		        	String xmllang = nodes.item(i).getAttributes().getNamedItem("lang").getNodeValue();
		        	if(xmllang.equals(lang)){
		        		content = nodes.item(i).getTextContent();
		        		break;
		        	}
	        	}
	        }
        
	    } catch (Exception ex) {
	    	ex.printStackTrace();
	    }
	    
	    return content;
	}

}
