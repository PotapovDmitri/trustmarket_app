package utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.HashSet;
import java.util.Set;

public class NumericUtils {

	public static final Set<String> badDoubles = new HashSet<String>() {
		{
			add("2.2250738585072012e-308"); // Java gets stuck, oscillating
											// between 0x1p-1022 and
											// 0x0.fffffffffffffp-1022, the
											// largest subnormal
											// double-precision floating-point
											// number.
			add("0.00022250738585072012e-304"); // decimal point placement
			add("00000000002.2250738585072012e-308"); // leading zeros
			add("2.225073858507201200000e-308"); // trailing zeros
			add("2.2250738585072012e-00308"); // leading zeros in the exponent
			add("2.2250738585072012997800001e-308"); // superfluous digits
														// beyond digit 17
		}
	};

	public static double tryToParseDouble(String val, double onFailure) {
		if (StringUtils.empty(val) || badDoubles.contains(val))
			return onFailure;
		try {
			val = val.replaceAll("[ ]", "");
			val = val.replaceAll("[,]", ".");
			return Double.parseDouble(val);
		} catch (Exception e) {
		}

		return onFailure;
	}
	
	
	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
	

}
