package utils;

public class AppConstants {
	public static final String EXECUTE_PAYMENT = "successepaypal";
	public static final String CANCEL_PAYMENT = "cancelpaypal";
	public static final String PAYPAL = "paypal";
	public static final String CREDIT_CARD = "credit_card";
	public static final String UTF_8 = "UTF-8";
	
	//public static final String BANK_DEFAULT_ENCODING = "UTF-8";
	public static final String BANK_DEFAULT_ENCODING = "ISO-8859-1";
	public static final String BANK_LANG = "EST";
	public static final String BANK_CURR = "EUR";
	public static final String BANK_DATETIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";
	

}
