package utils;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

public class FilesUtils {

	public static void createUserDirectories(Long userid) {

		createdirectory("public/files/user_" + userid);
		createdirectory("public/files/user_" + userid + "/invoices");
		createdirectory("public/files/user_" + userid + "/documents");
	}

	private static void createdirectory(String dir) {
		File userdir = new File(dir);
		boolean result = false;
		// if the directory does not exist, create it
		if (!userdir.exists()) {

			try {
				userdir.mkdir();
				result = true;
			} catch (SecurityException se) {
				// handle it
			}

			if (result) {
				System.out.println(dir + " DIR created");
			}else{
				System.out.println(dir + " DIR not created");
				
			}
		}
	}
	
	public static String deleteFile(String fileName, String directory){
		File file = new File(directory + "/" + fileName);
		System.out.println("delete file........filename:" + directory + "/" + fileName);

		if(!file.exists()){
			return "notexist";
		}
		if(file.delete()){
			return "deleted";
		}else{
			return "notdeleted";
		}
	}

	public static List<String> getFilesCode(Long requestid, Long userId){
		return getFilesCode(requestid, userId, false);
	}
	
	public static List<String> getFilesCode(Long requestid, Long userId, boolean withExt){
		List<String> codes = new ArrayList<String>();
		
		if(requestid != null && userId != null){
			String dir = "public/files/user_"+userId+"/documents";
			if(!new File(dir).exists()){
				System.out.println("System directories not exists for user:" + userId);
				createUserDirectories(userId);
			}
			String[] files = findFilesStartWithName("document_"+ requestid, dir);
			
			if(files == null){
				System.out.println("Requested file is not exist");
				return codes;
			}
			
			for(String file : files){
				String name = file.split("_")[1];
				name = name.substring(0, name.lastIndexOf("."));
				if(withExt){
					String ext = getFileExtension(file);
					name = name + (StringUtils.notEmpty(ext) ? "," + ext : ""); 
				}
				codes.add(name);
			}
			
		}
		
		return codes;
	}
	public static String[] findFilesStartWithName(final String fileName, String directory){
		return new File(directory).list(new FilenameFilter() { 
		    public boolean accept(File dir, String name) {
		        return name.startsWith(fileName); 
		    }
		});
	}
		
	public static String findNextFileName(final String fileName, String directory){
		String[] files = findFilesStartWithName(fileName, directory);
		
		return checkNextName(files, fileName);
		
		//return files.length == 0 ? fileName : fileName + "-"+files.length+"";
		
	}

	private static String checkNextName(String[] files, String nextName) {
		for(String file : files){
			file = file.substring(0, file.indexOf("."));
			if(file.equals(nextName)){
				String name = nextName.split("-")[0];
				if(nextName.split("-").length > 1){
					nextName = name + "-" + (Integer.parseInt(nextName.split("-")[1]) + 1);
				}else{
					nextName = name + "-1";
				}
				
				nextName = checkNextName(files, nextName);
			}
		}
		return nextName;
	}
	
	public static String findFileName(final String findFileName, String directory){
		System.out.println("findFileName..."+findFileName);
		String[] file =  new File(directory).list(new FilenameFilter() { 
		    public boolean accept(File dir, String filename) {
		    	String name  = filename.substring(0, filename.lastIndexOf("."));
		    	
		    	System.out.println("file..."+name);
		        return name.equals(findFileName); 
		    }
		});
		System.out.println("len..."+file.length);
		return file.length > 0 ? file[0] : null;
	}
	
	public static String getFileExtension(String  fileName) {
        if(fileName.lastIndexOf(".") > 0)
        return fileName.substring(fileName.lastIndexOf(".")+1);
        else return "";
    }
}
