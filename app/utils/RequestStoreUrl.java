package utils;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import forms.PossibleRequestForm;
import forms.RequestForm;
import play.i18n.Messages;
import models.Request;
import models.UserRequests;

public class RequestStoreUrl{

	
	public static String validateRequestStoreLink(Long userRequestId, Long requestId, String storeLink){
		
		String storeLinkHost = getHostName(storeLink);
		if(storeLinkHost == null)
			return Messages.get("storelink.wrong");

		System.out.println("................host name :"+storeLinkHost);
		if(userRequestId == null || userRequestId == 0)
			return "";
		
		List<Request> requests = UserRequests.findById(userRequestId).getRequest();
		
		if(!validateRequestStoreLinkHost(storeLinkHost, requests)){
			return Messages.get("storelink.wrongstore");
		}else if(!validateSameRequestStoreLink(requestId, storeLink, requests)){
			return Messages.get("storelink.same");
		}
		
		return "";
	}
	
	public static String validatePosibleRequestStoreLink(Long requestId, String storeLink, PossibleRequestForm posibleRequest){
		String storeLinkHost = getHostName(storeLink);
		if(storeLinkHost == null)
			return Messages.get("storelink.wrong");
		
		if(posibleRequest == null)
			return "";
		
		if(!validatePosibleRequestStoreLinkHost(requestId, storeLinkHost, posibleRequest.getRequests())){
			return Messages.get("storelink.wrongstore");
		}else if(!validateSamePosibleRequestStoreLink(requestId, storeLink, posibleRequest.getRequests())){
			return Messages.get("storelink.same");
		}
		return "";
	}
	
	
	/**
	 * Validate host to be sure that all links from same store
	 * 
	 * @param storeLink
	 * @param userRequestId
	 * @return
	 */
	private static boolean validateRequestStoreLinkHost(String storeLinkHost, List<Request> list){
		boolean validation = true;
		if(list != null && !list.isEmpty()){
			
			for(Request request : list){
				String host = getHostName(request.getStoreLink());
				if(host == null || !host.equals(storeLinkHost)){
					validation = false;
					break;
				}
			}
		}
		return validation;
	}
	
	private static boolean validatePosibleRequestStoreLinkHost(Long requestId, String storeLinkHost, List<RequestForm> list){
		boolean validation = true;
		if(list != null && !list.isEmpty()){
			
			for(RequestForm request : list){
				String host = getHostName(request.getStorelink());
				//failed if host is wrong or host not from same store
				if(host == null || (!host.equals(storeLinkHost) && requestId != request.getRequestId())){
					validation = false;
					break;
				}
			}
			
		}
		return validation;
	}
	
	/**
	 * Validate links to be sure that doesn't exists similar links
	 * 
	 * @param storeLink
	 * @param userRequestId
	 * @return
	 */
	private static boolean validateSameRequestStoreLink(Long requestId, String storeLink, List<Request> list){
		boolean validation = true;
		if(list != null && !list.isEmpty()){
			for(Request request : list){
				if(request.getStoreLink().equals(storeLink)){
					validation = false;
					break;
				}
			}
			
		}
		return validation;
	}
	private static boolean validateSamePosibleRequestStoreLink(Long requestId, String storeLink, List<RequestForm> list){
		boolean validation = true;
		if(list != null && !list.isEmpty()){
			for(RequestForm request : list){
				if(request.getStorelink().equals(storeLink) && (requestId == null || request.getRequestId() != requestId)){
					validation = false;
					break;
				}
			}
			
		}
		return validation;
	}
	
	private static String getHostName(String url){
		if(StringUtils.isEmpty(url))
			return null;
		try {
			URI uri = new URI(url);
			return uri.getHost();
		} catch (URISyntaxException ex) {
			  // validation failed
		}
		return null;
	}
}
