package utils;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import play.Play;

public class MailUtils {

	public enum Mails {
		MAIL_CUSTOMER_SEND_REQUEST("mail_customer_send_request"),
		MAIL_ADMIN_SEND_REQUEST("mail_admin_send_request"),
		MAIL_BILL_CREATED("mail_bill_created"),
		MAIL_BILL_PAYED("mail_bill_payed"),
		MAIL_BILL_UNPAYED("mail_bill_unpayed"),
		MAIL_MOREINFO("mail_moreinfo"),
		MAIL_SEND_MESSAGE("mail_send_message");
		
		private String mail;
		
		private Mails(String m){
			mail = m;
		}
		
		private String getMail(){
			return mail;
		}
	}
	
	public static String MAIL_CONTENT_FILE = "mail_templates.xml";
	
	public static String MAIL_CONTENT = "content";
	public static String MAIL_SUBJECT = "subject";
	
	public static Map<String, String> getMailContent(Mails mailtype){
		
		Map<String, String> mail = new HashMap<String, String>();
		
		// create a new DocumentBuilderFactory
	    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	    try {
			// use the factory to create a documentbuilder
	        DocumentBuilder builder = factory.newDocumentBuilder();
	        
			InputStream is = Play.application().resourceAsStream(MAIL_CONTENT_FILE);
			
			//FileInputStream fis = new FileInputStream("Student.xml");
	        Document doc = builder.parse(is);
	
	        // get the first element
	        Element element = doc.getDocumentElement();
	
	        // get all child nodes
	        NodeList nodes = element.getChildNodes();
	
	        // print the text content of each child
	        for (int i = 0; i < nodes.getLength(); i++) {
	           if(nodes.item(i).getNodeName().equals(mailtype.getMail())){
	        	   mail.put("content", nodes.item(i).getTextContent());
	        	   mail.put("subject", nodes.item(i).getAttributes().getNamedItem("subject").getNodeValue());
	        	   break;
	           }
	        }
        
	    } catch (Exception ex) {
	    	ex.printStackTrace();
	    }
	    
	    return mail;
	}

	public static void sendEmail(String text, String subject, String mailTo){
		
		
		// Load SMTP configuration
		String host = Play.application().configuration().getString("smtp.host");
		String port = Play.application().configuration().getString( "smtp.port" );
		final String username = Play.application().configuration().getString( "smtp.user" );
		final String password = Play.application().configuration().getString( "smtp.password" );

		System.out.println("Process Started");

		//TLC
		Properties tlcProps = new Properties();
		tlcProps.put("mail.smtp.host", "smtp.gmail.com");
		tlcProps.put("mail.smtp.socketFactory.port", "465");
		tlcProps.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		tlcProps.put("mail.smtp.auth", "true");
		tlcProps.put("mail.smtp.port", "465");
		
		//SSL
		Properties sslProps = new Properties();
		sslProps.put("mail.smtp.auth", "true");
		sslProps.put("mail.smtp.starttls.enable", "true");
		sslProps.put("mail.smtp.host", "smtp.gmail.com");
		sslProps.put("mail.smtp.port", "587");
		
        Properties prop = System.getProperties();
        prop.setProperty("mail.smtp.starttls.enable", "true");
        prop.setProperty("mail.smtp.host", host);
        prop.setProperty("mail.smtp.user", username);
        prop.setProperty("mail.smtp.password", password);
        prop.setProperty("mail.smtp.port", port);
        prop.setProperty("mail.smtp.auth", "true");
        
        System.out.println("mail to : " + mailTo);
        System.out.println("text : " + text);
        System.out.println("subject : " + subject);

        javax.mail.Session session = javax.mail.Session.getInstance(prop, new javax.mail.Authenticator() {
        	protected PasswordAuthentication getPasswordAuthentication() {
        		return new PasswordAuthentication(username, password);
        	}
        });

        MimeMessage message = new MimeMessage(session);
        try {
            System.out.println("before sending");
            message.setFrom(new InternetAddress(username));
            
            message.setSubject(subject);
            message.setText(text);
            
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mailTo));
            
            Transport.send(message);
            
/*            Transport transport = session.getTransport("smtp");
            System.out.println("Got Transport" + transport);
            transport.connect(SMTP_HOST, username, password);
            transport.sendMessage(message, message.getAllRecipients());
*/            
            System.out.println("message Object : " + message);
            System.out.println("Email Sent Successfully");
            
        } catch (AddressException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (MessagingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
	}
}
