package utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validations {
	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	 
	public static boolean validateEmailAddress(final String emailAddress) {
		
		Pattern regexPattern = Pattern.compile(EMAIL_PATTERN);
		Matcher regMatcher = regexPattern.matcher(emailAddress);
		if (regMatcher.matches()) {
			return true;
		} else {
			return false;
		}
	}

	public boolean validateMobileNumber(String mobileNumber) {
		Pattern regexPattern = Pattern.compile("^\\+[0-9]{2,3}+-[0-9]{10}$");
		Matcher regMatcher = regexPattern.matcher(mobileNumber);
		if (regMatcher.matches()) {
			return true;
		} else {
			return false;
		}

	}
}
