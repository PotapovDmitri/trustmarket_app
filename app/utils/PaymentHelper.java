package utils;

import java.util.ArrayList;
import java.util.List;

import models.Bill;
import models.Request.RequestType;
import models.UserRequests;

import com.paypal.api.payments.Amount;
import com.paypal.api.payments.Details;
import com.paypal.api.payments.Payer;
import com.paypal.api.payments.Payment;
import com.paypal.api.payments.RedirectUrls;
import com.paypal.api.payments.Transaction;
import com.paypal.core.rest.APIContext;
import com.paypal.core.rest.PayPalRESTException;

public class PaymentHelper {

	public static Payment createPayment(UserRequests req, String guid) throws PayPalRESTException {
		
		Bill bill = req.getBill();
		
		//Details
		Details details = new Details();
		if(RequestType.FULL.toString().equals(req.getRequestsType())){
			details.setShipping(MoneyUtils.formatPrice(bill.getDeliverySumma()));
			details.setSubtotal(MoneyUtils.formatPrice(bill.getProductSumma() + bill.getServiceSumma()));
		}else{
			details.setSubtotal(MoneyUtils.formatPrice(bill.getServiceSumma()));
		}
		details.setTax("0");

		//Amount
		Amount amount = new Amount();
		amount.setCurrency("EUR");
		// Total must be equal to sum of shipping, tax and subtotal.
		amount.setTotal(MoneyUtils.formatPrice(bill.getTotalSumma()));
		amount.setDetails(details);

		//Transaction
		Transaction transaction = new Transaction();
		transaction.setAmount(amount);
		transaction.setDescription("Trustmarket.eu makse - arve nr: "+bill.getBillId()+" / päring nr: " + req.getUserRequestId());

		List<Transaction> transactions = new ArrayList<Transaction>();
		transactions.add(transaction);

		//Payer
		Payer payer = new Payer();
		payer.setPaymentMethod("paypal");

		//Payment
		Payment payment = new Payment();
		payment.setIntent("sale");
		payment.setPayer(payer);
		payment.setTransactions(transactions);
		
		
		RedirectUrls redirectUrls = new RedirectUrls();
		//redirectUrls.setCancelUrl("http://107.170.22.180:9000/user/cancelpaypal?guid=" + guid);
		//redirectUrls.setReturnUrl("http://107.170.22.180:9000/user/successepaypal?guid=" + guid);
		redirectUrls.setCancelUrl("http://localhost:9000/user/cancelpaypal?guid=" + guid + "&billid=" + req.getBill().getBillId());
		redirectUrls.setReturnUrl("http://localhost:9000/user/successepaypal?guid=" + guid + "&billid=" + req.getBill().getBillId());

		//redirectUrls.setCancelUrl("https://trustmarket.eu/user/cancelpaypal?guid=" + guid + "&billid=" + req.getBill().getBillId());
		//redirectUrls.setReturnUrl("http://trustmarket.eu/user/successepaypal?guid=" + guid + "&billid=" + req.getBill().getBillId());
		payment.setRedirectUrls(redirectUrls);
		
		// set access token
		String accessToken = AccessTokenGenerator.getAccessToken();
		APIContext apiContext = new APIContext(accessToken);
		
		return payment.create(apiContext);
	}
}
