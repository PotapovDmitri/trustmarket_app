package utils;

import java.net.URI;
import java.net.URISyntaxException;

public class StringUtils {

	public static boolean isEmpty(String str) {

		if (str == null || str.isEmpty()) {
			return true;
		}
		return false;
	}

	public static boolean notEmpty(String s) {
		if (s == null || s.trim().length() < 1)
			return false;

		return true;
	}

	public static boolean empty(String s) {
		return !notEmpty(s);
	}

	public static String substring(String str, int num) {

		if (str != null && str.length() > num) {
			return str.substring(0, num) + "...";
		}
		return str;
	}

	public static String getDomainName(String url) throws URISyntaxException {
		URI uri = new URI(url);
		String domain = uri.getHost();
		return domain.startsWith("www.") ? domain.substring(4) : domain;
	}

	public String getUrlDomainName(String url) {
		String domainName = new String(url);

		int index = domainName.indexOf("://");

		if (index != -1) {
			// keep everything after the "://"
			domainName = domainName.substring(index + 3);
		}

		index = domainName.indexOf('/');

		if (index != -1) {
			// keep everything before the '/'
			domainName = domainName.substring(0, index);
		}

		// check for and remove a preceding 'www'
		// followed by any sequence of characters (non-greedy)
		// followed by a '.'
		// from the beginning of the string
		domainName = domainName.replaceFirst("^www.*?\\.", "");

		return domainName;
	}
}
