package utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {

	
	public static String parseDateToString(Date date){
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
		return date != null ? dateFormat.format(date) : "";
	}
	
	public static Date parseDate(Date date){
		String datestr = parseDateToString(date);
		SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        try {
			return formatter.parse(datestr);
		} catch (ParseException e) {
		}
        return null;
	}
}
