package utils;

import com.paypal.core.ConfigManager;
import com.paypal.core.rest.OAuthTokenCredential;
import com.paypal.core.rest.PayPalRESTException;

public class AccessTokenGenerator {
	private static String accessToken;

	public static String getAccessToken() throws PayPalRESTException {
		// ###AccessToken
		// Retrieve the access token from
		// OAuthTokenCredential by passing in
		// ClientID and ClientSecret
		if (accessToken == null) {
			// ClientID and ClientSecret retrieved from configuration
			String clientID = "AZzl9xCJRguGJEBeJVZ05c8Una6ShXluebqtMcFzMXnyo6G4a74o9CDw-sRV";
			String clientSecret = "EAuYwhDplZIkCn-pszISzFtSvA_ElldQ4xBTp3ggTKo3pvhgIZYh2BFlVjnX";
			//String clientID = ConfigManager.getInstance().getValue("clientID");
			//String clientSecret = ConfigManager.getInstance().getValue("clientSecret");
			accessToken = new OAuthTokenCredential(clientID, clientSecret).getAccessToken();
		}
		return accessToken;
	}
}
