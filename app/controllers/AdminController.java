package controllers;

import static play.data.Form.form;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import models.AdminStores;
import models.Bill;
import models.Bill.BillStates;
import models.Request;
import models.RequestMessages;
import models.User;
import models.UserRequests;

import org.apache.commons.io.FileUtils;

import play.data.Form;
import play.i18n.Messages;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import util.pdf.PDF;
import utils.FilesUtils;
import utils.MailUtils;
import utils.MailUtils.Mails;
import utils.StringUtils;

import com.fasterxml.jackson.databind.node.ObjectNode;

import forms.BillForm;
import forms.MessageForm;

@Security.Authenticated(Secured.class)
public class AdminController extends Controller {
	
	public static Result index() {
		
		User user = User.findByUsername(request().username());
		
		if(Secured.isUser()){
			return redirect(routes.UserController.index());
		}
		
		List<User> users = User.findUsers();

		List<UserRequests> requests = UserRequests.findUsersCreatedRequests();
		List<UserRequests> historyRequests = UserRequests.findUsersHistoryRequests();

		List<Bill> bills = Bill.findAll();
		List<AdminStores> stores = AdminStores.findAll();
		
		int created = 0, payed = 0, unpyaed = 0, canceled = 0;
		for(Bill b: bills){
			if(BillStates.CREATED.toString().equals(b.getBillState())){
				created ++;
			}else if(BillStates.PAYED.toString().equals(b.getBillState())){
				payed++;
			}else if(BillStates.UNPAYED.toString().equals(b.getBillState())){
				unpyaed++;
			}else if(BillStates.CANCELED.toString().equals(b.getBillState())){
				canceled++;
			}
		}
		
		return ok(views.html.admin.render(user, users, requests, historyRequests, bills, stores, created, payed, unpyaed, canceled));
	}
	
	public static Result admchangelang(String lang) {
		if(!StringUtils.isEmpty(lang)){
			changeLang(lang);
		}
		System.out.println("...........admin change lang");

		return redirect(routes.AdminController.index());
	}

	public static Result createbill(){
		Form<BillForm> form = form(BillForm.class).bindFromRequest();
		ObjectNode result = Json.newObject();
		
		
		System.out.println("........" +Messages.get("pdf.billidtext"));
		if (form.hasErrors()) {
			return forbidden(form.errorsAsJson());
		}else{
			BillForm billform = form.get();
			Bill bill = Bill.createBill(billform);
			User billuser = bill.getUserRequests().getUser();
			try {
				createInvoice(bill, billuser.getUserId());
			
			} catch (IOException e) {
				e.getStackTrace();	
			}
			
			//send message to admin
		/*	Map<String, String> adminmail = MailUtils.getMailContent(Mails.MAIL_BILL_CREATED);
			
			String admincontent = adminmail.get(MailUtils.MAIL_CONTENT);
			admincontent = admincontent.replace("<user>", billuser.getFirstName() + " " + billuser.getLastName());
			admincontent = admincontent.replace("<billid>", String.valueOf(bill.getBillId()));
			admincontent = admincontent.replace("<requestid>", String.valueOf(bill.getUserRequests().getUserRequestId()));
			admincontent = admincontent.replace("<totalprice>", String.valueOf(MoneyUtils.formatPrice(bill.getTotalSumma())));
			
			MailUtils.sendEmail(admincontent, adminmail.get(MailUtils.MAIL_SUBJECT), billuser.getEmail());
			*/
			result.put("status", "OK");
			return ok(result);
			
		}
	}

	private static void createInvoice(Bill bill, Long userId) throws IOException {

        if(!new File("public/files/user_"+userId+"/invoices").exists()){
        	System.out.println("Creating default directories for user:" + userId);
        	FilesUtils.createUserDirectories(userId);
        }

		byte[] bytes = PDF.toBytes(views.html.invoicepdf.render(bill));
		 FileOutputStream output;
		try{
		File file = new File("invoice_"+bill.getBillId()+".pdf");
        if (file.exists()) {
            file.delete();
        }
        output = new FileOutputStream(file);
        BufferedOutputStream bos = new BufferedOutputStream(output);
        bos.write(bytes);

        bos.flush();
        bos.close();

        output.flush();
        output.close();
        /*
        FileOutputStream fos = new FileOutputStream(file);
        fos.write(bytes);
        fos.flush();
        fos.close();
        */
		
		FileUtils.moveFile(file, new File("public/files/user_"+userId+"/invoices", "invoice_"+bill.getBillId()+".pdf"));
		} catch (FileNotFoundException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	}
	
	public static Result sendanswermessage(){
		User user = User.findByUsername(request().username());
		List<User> users = User.findUsers();

		List<UserRequests> requests = UserRequests.findUsersCreatedRequests();
		List<UserRequests> historyRequests = UserRequests.findUsersHistoryRequests();

		List<Bill> bills = Bill.findAll();
		List<AdminStores> stores = AdminStores.findAll();
		
		Form<MessageForm> form = form(MessageForm.class).bindFromRequest();
		if (form.hasErrors()) {
			int created = 0, payed = 0, unpyaed = 0, canceled = 0;
			for(Bill b: bills){
				if(BillStates.CREATED.toString().equals(b.getBillState())){
					created ++;
				}else if(BillStates.PAYED.toString().equals(b.getBillState())){
					payed++;
				}else if(BillStates.UNPAYED.toString().equals(b.getBillState())){
					unpyaed++;
				}else if(BillStates.CANCELED.toString().equals(b.getBillState())){
					canceled++;
				}
			}
			return badRequest(views.html.admin.render(user, users, requests, historyRequests, bills, stores, created, payed, unpyaed, canceled));
		}else{
			MessageForm mform = form.get();
			RequestMessages.createMessage(mform, user);
			
			Map<String, String> mail = MailUtils.getMailContent(Mails.MAIL_SEND_MESSAGE);
			
			UserRequests ur = UserRequests.findById(mform.getUserrequestid());
			
			String toemail = ur.getUser().getEmail();
			String content = mail.get(MailUtils.MAIL_CONTENT);
			content = content.replace("<user>", ur.getUser().getFirstName() + " " + ur.getUser().getLastName());
			content = content.replace("<fromuser>", "Admin");
			content = content.replace("<requestid>", String.valueOf(mform.getUserrequestid()));
			content = content.replace("<content>", mform.getMessage());
			
			MailUtils.sendEmail(content, mail.get(MailUtils.MAIL_SUBJECT), toemail);
			
			return redirect(routes.AdminController.index());
		}
	}
	
	public static Result markpayed(Long billid){
		
		ObjectNode result = Json.newObject();

		if(billid > 0){

			Bill bill = Bill.findById(billid);
			if(bill != null){

				Bill.updateBillState(billid, BillStates.PAYED);
				result.put("status", "OK");
			}else{
				result.put("status", "FAILUER");
				result.put("message", Messages.get("bill.notexist"));
			}
		}
		
		return ok(result);
	}
	
	public static Result markcanceled(Long billid){
		
		ObjectNode result = Json.newObject();

		if(billid > 0){

			Bill bill = Bill.findById(billid);
			if(bill != null){

				Bill.updateBillState(billid, BillStates.CANCELED);
				result.put("status", "OK");
			}else{
				result.put("status", "FAILUER");
				result.put("message", Messages.get("bill.notexist"));
			}
		}
		
		return ok(result);
	}
	
	public static Result getdoc(String requestcode){
		
		if(StringUtils.isEmpty(requestcode)){
			return forbidden("Something wrong with request code");
		}
		Long requestid = Long.parseLong(requestcode.split("-")[0]);
		Request request = Request.findById(requestid);
		if(request == null){
			return forbidden("Something wrong with request");
		}
		String dir = "public/files/user_"+request.getUserRequests().getUser().getUserId()+"/documents";
		String doc = "document_"+ requestcode;
		String file = FilesUtils.findFileName(doc, dir);
		if(file != null){
			return ok(new File(dir, file));
		}
		
		return forbidden("DOCUMENT '"+doc+"' NOT FOUND");
	}
	
}
