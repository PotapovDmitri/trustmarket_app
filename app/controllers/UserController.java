package controllers;

import static play.data.Form.form;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import models.Bill;
import models.Bill.BillStates;
import models.Request;
import models.RequestMessages;
import models.User;
import models.UserRequests;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;

import play.Play;
import play.data.Form;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http.MultipartFormData;
import play.mvc.Http.MultipartFormData.FilePart;
import play.mvc.Result;
import play.mvc.Security;
import services.payments.ConfigurableIBanksService;
import services.payments.IBanksException;
import services.payments.IBanksUtils;
import services.payments.messages.IBanksMessage1012;
import services.payments.messages.IBanksMessage1111;
import utils.AccessTokenGenerator;
import utils.AppConstants;
import utils.DateUtils;
import utils.FilesUtils;
import utils.MD5Hash;
import utils.MailUtils;
import utils.MailUtils.Mails;
import utils.MoneyUtils;
import utils.NumericUtils;
import utils.PaymentHelper;
import utils.RequestStoreUrl;
import utils.StringUtils;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.paypal.api.payments.Links;
import com.paypal.api.payments.Payment;
import com.paypal.api.payments.PaymentExecution;
import com.paypal.core.rest.APIContext;
import com.paypal.core.rest.PayPalRESTException;

import forms.MessageForm;
import forms.RequestForm;
import forms.UserForm;

@Security.Authenticated(Secured.class)
public class UserController extends Controller {

	private static Map<String, String> map;
	
	static{
		map = new HashMap<String, String>();
	}
	
	public static Result index() {
		if(Secured.isAdmin()){
			return redirect(routes.AdminController.index());
		}

		User user = User.findByUsername(request().username());
		List<UserRequests> requests = UserRequests.findUserCreatedRequests(user.getUserId());
		List<UserRequests> historyRequests = UserRequests.findUserHistoryRequests(user.getUserId());
	    
		return ok(views.html.user.render(user, requests, historyRequests, form(RequestForm.class), form(MessageForm.class)));
	}
	
	public static Result testmail(){
		Map<String, String> mail = MailUtils.getMailContent(Mails.MAIL_SEND_MESSAGE);
		
		String toemail = Play.application().configuration().getString("smtp.user");
		String content = mail.get(MailUtils.MAIL_CONTENT);
		content = content.replace("<user>", "Admin");
		content = content.replace("<fromuser>", "Dmitri Potapov");
		content = content.replace("<requestid>", "1");
		content = content.replace("<content>", "test message");
		
		MailUtils.sendEmail(content, mail.get(MailUtils.MAIL_SUBJECT), toemail);
		
		return redirect(routes.UserController.index());
	}
	
	public static Result usrchangelang(String lang) {
		if(!StringUtils.isEmpty(lang)){
			changeLang(lang);
		}
		return redirect(routes.UserController.index());
	}
	
	public static Result createrequest() throws IOException {
	    
		User user = User.findByUsername(request().username());

		Form<RequestForm> form = form(RequestForm.class).bindFromRequest();
		if (form.hasErrors()) {
			return badRequest(form.errorsAsJson());
		} else {
			ObjectNode result = Json.newObject();
			if("1".equals(form.get().getType())){
				//validate store url
				String storeListValidation = RequestStoreUrl.validateRequestStoreLink(form.get().getUserRequestId(), form.get().getRequestId(), form.get().getStorelink());
				if(StringUtils.notEmpty(storeListValidation)){
					return badRequest("{\"errors\":[\"storelink;"+storeListValidation+"\"]}");
				}
			}
			
			Request req = null;
			
			if(form.get().getRequestId() == 0){
				//NEW REQUEST
				UserRequests userrequest = null;
				if(form.get().getUserRequestId() == 0){
					userrequest = UserRequests.createUserRequest(user);
				}else{
					userrequest = UserRequests.findById(form.get().getUserRequestId());
				}
				System.out.println(".........new request");
				req = Request.createRequest(form.get(), userrequest, user);
			}else{
				//UPDATE REQUEST
				System.out.println(".........update request : " + form.get().getRequestId());
				req = Request.updateRequest(form.get());
			}
			
			
			//DELIVERY
			if("2".equals(form.get().getType())){
				MultipartFormData body = request().body().asMultipartFormData();
				FilePart document = body.getFile("document");
				if(document != null){
					String directory = "public/files/user_"+user.getUserId()+"/documents";
					String fileName = FilesUtils.findNextFileName("document_"+ req.getRequestId(), directory);
					String extention = FilesUtils.getFileExtension(document.getFilename());
					
					System.out.println("FILE NAME : "+fileName+"." + extention);
					File file = new File(directory, fileName + "." + extention);
					FileUtils.moveFile(document.getFile(), file);
				}
			}
			result.put("result", req.getRequestId());
			result.put("status", "OK");
			return ok(result);
		}
	}

	public static Result deleterequest(Long requestid) {
		ObjectNode result = Json.newObject();
		String userRequestId = form().bindFromRequest().get("userrequestid");

		Request.deleteRequest(requestid);
		
		UserRequests request = UserRequests.findById(Long.parseLong(userRequestId));
		if(request.getRequest().isEmpty()){
			result.put("deletestatus", "empty");
			//delete user request
			UserRequests.delete(Long.valueOf(userRequestId));
		}
		result.put("status", "OK");
		result.put("requestid", requestid);
		result.put("userrequestid", userRequestId);
		
		result.put("commissionprice", MoneyUtils.formatPrice(request.getCommissionPrice()));
		result.put("totalprice", MoneyUtils.formatPrice(request.getTotalPrice()));
		
		return ok(result);
	}
	

	
	public static Result getrequest(Long requestid) {
		ObjectNode result = Json.newObject();
		Request request = Request.findById(requestid);
		if(request != null){
			result.put("status", "OK");
			result.put("requesttype", request.getType());
			if("delivery".equals(request.getType().toLowerCase())){
				result.put("packagecode", request.getPackageCode());
				String codes = "";
				for (int i = 0; i < request.getDocuments().size(); i++) {
					codes += (i > 0 ? "," : "")+request.getDocuments().get(i);
				}
				result.put("doc_codes", codes);
			}else if("full".equals(request.getType().toLowerCase())){
				result.put("requestname", request.getName());
				result.put("requestlink", request.getStoreLink());
				result.put("requestcolor", request.getColor());
				result.put("requestcount", request.getCount());
				result.put("requestsize", request.getSize());
				result.put("requestarticle", request.getArticle());
			}
			
			result.put("requestprice", MoneyUtils.formatPrice(request.getPrice()));
			result.put("requestdescription", request.getDescription());
		}else{
			result.put("status", "FAILURE");
		}
		
		result.put("requestid", requestid);
		result.put("userrequestid", request.getUserRequests().getUserRequestId());

		return ok(result);
	}
	
	public static Result editdeliveryprice(Long userrequestid){
		double deliveryprice = NumericUtils.tryToParseDouble(form().bindFromRequest().get("deliveryprice"), 0.0);
		UserRequests request = UserRequests.updateDeliveryPrice(userrequestid, deliveryprice);

		ObjectNode result = Json.newObject();
		result.put("status", "OK");
		result.put("userrequestid", userrequestid);
		result.put("deliveryprice", MoneyUtils.formatPrice(request.getDeliveryPrice()));
		result.put("totalprice", MoneyUtils.formatPrice(request.getTotalPrice()));
		return ok(result);
	}
	
	public static Result sendrequest(Long userrequestid){
		User user = User.findByUsername(request().username());
		ObjectNode result = Json.newObject();
		
		UserRequests ureq = UserRequests.sendrequest(userrequestid);
		
		//send message to customer
		Map<String, String> cumail = MailUtils.getMailContent(Mails.MAIL_CUSTOMER_SEND_REQUEST);
		
		String cucontent = cumail.get(MailUtils.MAIL_CONTENT);
		cucontent = cucontent.replace("<user>",  user.getFirstName() + " " + user.getLastName());
		cucontent = cucontent.replace("<requestid>", String.valueOf(userrequestid));
		cucontent = cucontent.replace("<totalprice>", String.valueOf(MoneyUtils.formatPrice(ureq.getTotalPrice())));
		
		MailUtils.sendEmail(cucontent, cumail.get(MailUtils.MAIL_SUBJECT), user.getEmail());
		
		
		//send message to admin
		Map<String, String> adminmail = MailUtils.getMailContent(Mails.MAIL_ADMIN_SEND_REQUEST);
		
		String admincontent = adminmail.get(MailUtils.MAIL_CONTENT);
		admincontent = admincontent.replace("<fromuser>", user.getFirstName() + " " + user.getLastName());
		admincontent = admincontent.replace("<requestid>", String.valueOf(userrequestid));
		admincontent = admincontent.replace("<totalprice>", String.valueOf(MoneyUtils.formatPrice(ureq.getTotalPrice())));
		
		MailUtils.sendEmail(admincontent, adminmail.get(MailUtils.MAIL_SUBJECT), Play.application().configuration().getString("smtp.user"));
		
		result.put("status", "OK");
		result.put("userrequestid", userrequestid);
		return ok(result);
	}
	
	public static Result getcurrentuser(){
		User user = User.findByUsername(request().username());
		ObjectNode result = Json.newObject();
		
		if(user != null){
			result.put("status", "OK");
			result.put("id", user.getUserId());
			result.put("username", user.getUsername());
			result.put("email", user.getEmail());
			result.put("firstname", user.getFirstName());
			result.put("lastname", user.getLastName());
			result.put("birthdate", DateUtils.parseDateToString(user.getBirthday()));
			result.put("passportnr", user.getPassportNr());
		}else{
			result.put("status", "FAILURE");
		}
		return ok(result);
	}

	public static Result updatecurrentuser(){
		
	    
		Form<UserForm> form = form(UserForm.class).bindFromRequest();
		ObjectNode result = Json.newObject();
		
		if (form.hasErrors()) {
			result.put("status", "FAILURE");
			result.put("errors", form.errorsAsJson());
		} else {
			User.updateUser(form.get());
			result.put("status", "OK");
		}
		return ok(result);
	}
	
	
	public static Result sendquetionmessage(){
		User user = User.findByUsername(request().username());
		List<UserRequests> requests = UserRequests.findUserCreatedRequests(user.getUserId());
		List<UserRequests> historyRequests = UserRequests.findUserHistoryRequests(user.getUserId());

		Form<MessageForm> form = form(MessageForm.class).bindFromRequest();
		if (form.hasErrors()) {
			return badRequest(views.html.user.render(user, requests, historyRequests, form(RequestForm.class), form));
		}else{
			MessageForm mform = form.get();

			RequestMessages.createMessage(mform, user);

			Map<String, String> mail = MailUtils.getMailContent(Mails.MAIL_SEND_MESSAGE);
			
			String toemail = Play.application().configuration().getString("smtp.user");
			String content = mail.get(MailUtils.MAIL_CONTENT);
			content = content.replace("<user>", "Admin");
			content = content.replace("<fromuser>", user.getFirstName() + " " + user.getLastName());
			content = content.replace("<requestid>", String.valueOf(mform.getUserrequestid()));
			content = content.replace("<content>", mform.getMessage());
			
			MailUtils.sendEmail(content, mail.get(MailUtils.MAIL_SUBJECT), toemail);
			
			return redirect(routes.UserController.index());
		}
	}
	
	public static Result getusrdoc(String requestcode){
		User user = User.findByUsername(request().username());
		String dir = "public/files/user_"+user.getUserId()+"/documents";
		String doc = "document_"+ requestcode;
		String file = FilesUtils.findFileName(doc, dir);
		if(file != null){
			return ok(new File(dir, file));
		}
		
		return forbidden("DOCUMENT '"+doc+"' NOT FOUND");
	}
	
	

	public static Result deleterequestfile(String code) {
		ObjectNode result = Json.newObject();
		User user = User.findByUsername(request().username());
		if(StringUtils.notEmpty(code)){
			String directory = "public/files/user_"+user.getUserId()+"/documents";
			String fileName = FilesUtils.findFileName("document_"+ code, directory);
			System.out.println("delete file.....code:"+code+"...filename:" + fileName);
			String status = FilesUtils.deleteFile(fileName, directory);
			result.put("status", "OK");
			result.put("del_status", status);
		}else{
			result.put("status", "FAILURE");
			
		}
		return ok(result);
	}
	public static Result getinvoice(Long billid){
		User user = User.findByUsername(request().username());
		System.out.println("public/files/user_"+user.getUserId()+"/invoices/invoice_" + billid + ".pdf");
		File invoice = new File("public/files/user_"+user.getUserId()+"/invoices", "invoice_" + billid + ".pdf");
		if(invoice.exists()){
			return ok(invoice);
		}
		
		return forbidden("INVOICE 'invoice_" + billid + ".pdf' NOT FOUND");
	}
	
	/**
	 * Bank payment
	 * 
	 * @param userrequestid
	 * @param bank
	 * @return
	 */
	public static Result bankpay(Long userrequestid, String bank){
		try {
			
			System.out.println("Requested bank is '" + bank + "'");
			
			ConfigurableIBanksService service = ConfigurableIBanksService.getInstance(bank);
			
			UserRequests req = UserRequests.findById(userrequestid);
			Integer totalprice = req.getBill().getTotalSumma();
			IBanksMessage1012 msg = new IBanksMessage1012(String.valueOf(req.getBill().getBillId()),
					MoneyUtils.formatPrice(totalprice),"", "Trustmarket.eu makse - arve nr: "+req.getBill().getBillId());

			msg.setVK_SND_ID(service.getVkRecId());
			msg.setVK_RETURN(service.getReturnUrl1111());
			msg.setVK_CANCEL(service.getCancelUrl1911());
			
			service.sign(msg);
			
			response().setContentType("text/html");
			String html = IBanksUtils.messageToHTMLForm(service, msg, true);
	
			return ok(html);

		} catch (IBanksException e){
			e.printStackTrace();
		}
		return forbidden();
	}
	
	public static Result okbank(){
		Map<String, String[]> params = request().body().asFormUrlEncoded();
		
		try{
		IBanksMessage1111 msg = new IBanksMessage1111(params.get("VK_REC_ID")[0], params.get("VK_STAMP")[0], params.get("VK_T_NO")[0], params.get("VK_AMOUNT")[0],
				params.get("VK_REC_ACC")[0], params.get("VK_REC_NAME")[0], params.get("VK_SND_ID")[0], params.get("VK_SND_ACC")[0], params.get("VK_SND_NAME")[0], params.get("VK_REF")[0],
				params.get("VK_MSG")[0], params.get("VK_T_DATETIME")[0]);
		
		ConfigurableIBanksService service = ConfigurableIBanksService.getInstance("SWEDBANK");
		msg.setVK_MAC(params.get("VK_MAC")[0]);
		
		String paymsg = "ok";
		if(service.verify(msg)){
			Bill.updateBillState(Long.parseLong(params.get("VK_STAMP")[0]), BillStates.PAYED);
		}else{
			Bill.updateBillState(Long.parseLong(params.get("VK_STAMP")[0]), BillStates.UNPAYED);
			paymsg = "not";
		}
		
		
		//return redirect(routes.UserController.index());
		return redirect("/user?pay="+paymsg);
	} catch (IBanksException e){
		e.printStackTrace();
	}
		return forbidden();
	}
	
	public static Result cancelbank(){
		Map<String, String[]> params = request().body().asFormUrlEncoded();
		Long billId = Long.parseLong(params.get("VK_STAMP")[0]);
		if(billId != null){
			Bill.updateBillState(billId, BillStates.CANCELED);
		}
		//return redirect(routes.UserController.index());
		return redirect("/user?pay=not");
	}

	/**
	 * WebMoney pay
	 * 
	 * @return
	 */
	public static Result paywm(Long userrequestid){
	
		response().setContentType("text/html");
		UserRequests req = UserRequests.findById(userrequestid);
		
		String html = webMoneyMessageToHTMLForm(req);
		return ok(html);
	}
	
	public static String webMoneyMessageToHTMLForm(UserRequests req){
		StringBuffer buf = new StringBuffer();
	      
	      buf.append("<html>\n");
	      buf.append("<body>\n");
	      buf.append("<form id=\"pay\" name=\"pay\" method=\"POST\" target=\"_self\" action=\"");
	      buf.append("https://merchant.webmoney.ru/lmi/payment.asp");
	      buf.append("\">\n");
	      
	      //buf.append(IBanksUtils.toHTMLHidden("LMI_PAYMENT_AMOUNT", MoneyUtils.formatPrice(req.getBill().getTotalSumma())));
	      buf.append(IBanksUtils.toHTMLHidden("LMI_PAYMENT_AMOUNT", "0.10"));
	      buf.append(IBanksUtils.toHTMLHidden("LMI_PAYMENT_DESC_BASE64", Base64.decodeBase64("Trustmarket.eu оплата по счету: "+req.getBill().getBillId()) + ""));
	      buf.append(IBanksUtils.toHTMLHidden("LMI_PAYMENT_NO", String.valueOf(req.getBill().getBillId())));
	      buf.append(IBanksUtils.toHTMLHidden("LMI_PAYEE_PURSE", "E235598496361"));
	      buf.append(IBanksUtils.toHTMLHidden("LMI_SIM_MODE", "0"));
	      buf.append(IBanksUtils.toHTMLHidden("LMI_MODE", "test"));
	      buf.append(IBanksUtils.toHTMLHidden("ADD_PARAM", String.valueOf(req.getBill().getBillId())));
	      
	    
	        buf.append("<script>");
	        buf.append("document.forms[0].submit();");
	        buf.append("</script>\n");
	      buf.append("</form>\n");
	      buf.append("</body>\n");
	      buf.append("</html>\n");
	      return buf.toString();
	}
	
	/**
	 * Verify WebMoney payment request
	 * 
	 * @return
	 */
	public static Result verwm(){
		Map<String, String[]> params = request().body().asFormUrlEncoded();

		Long requestid = Long.valueOf(params.get("LMI_PREREQUEST")[0]);
		if(requestid != null && requestid == 1){
			Long billid1 = Long.valueOf(params.get("LMI_PAYMENT_NO")[0]);
			Long billid2 = Long.valueOf(params.get("ADD_PARAM")[0]);
			
			String purse = params.get("LMI_PAYEE_PURSE")[0];
			String price = params.get("LMI_PAYMENT_AMOUNT")[0];
			
			//Bill bill = Bill.findById(billid1);
			//String billprice = MoneyUtils.formatPrice(bill.getTotalSumma());
			String billprice = "0.10";
			
			if("E235598496361".equals(purse) && (billid1 != null && billid2 != null && billid2 == billid1) && billprice.equals(price)){
				return ok("YES");
			}else{
				return forbidden();
			}
		}else{
			String md5str = MD5Hash.md5Java(params.get("LMI_PAYEE_PURSE")[0] + params.get("LMI_PAMENT_AMOUNT")[0] + params.get("LMI_PAYMENT_NO")[0] +
					params.get("LMI_MODE")[0] + params.get("LMI_SYS_INVS_NO")[0] + params.get("LMI_SYS_TRANS_NO")[0] +
					params.get("LMI_SYS_TRANS_DATE")[0] + "grangarwmoney" + params.get("LMI_PAYER_PURSE")[0] + params.get("LMI_PAYER_WM")[0]);
			if(md5str.equals(params.get("LMI_HASH")[0])){
				Bill.updateBillState(Long.valueOf(params.get("LMI_PAYMENT_NO")[0]), BillStates.PAYED);
				return ok("YES");
			}else{
				return forbidden();
			}
		}
		
		//return redirect(routes.UserController.index());
	}
	
	/**
	 * WebMoney ok response
	 * 
	 * @return
	 */
	public static Result okwm(){
		Map<String, String[]> params = request().body().asFormUrlEncoded();
		Long billid1 = Long.valueOf(params.get("LMI_PAYMENT_NO")[0]);
		Long billid2 = Long.valueOf(params.get("ADD_PARAM")[0]);
		String msg = "not";
		if(billid1 != null && billid2 != null && billid2 == billid1){
			//Bill.updateBillState(billid1, BillStates.PAYED);
			msg = "ok";
			//otoslat pismo ob udachnoi oplate
		}
		//return redirect(routes.UserController.index());
		return redirect("/user?pay="+msg);
	}
	/**
	 * WebMoney cancel response
	 * 
	 * @return
	 */
	public static Result cancelwm(){
		Map<String, String[]> params = request().body().asFormUrlEncoded();
		Long billid1 = Long.valueOf(params.get("LMI_PAYMENT_NO")[0]);
		Long billid2 = Long.valueOf(params.get("ADD_PARAM")[0]);
		if(billid1 != null && billid2 != null && billid2 == billid1){
			Bill.updateBillState(billid1, BillStates.UNPAYED);
		}
		//DefaultRequestBody(Some(Map(LMI_LANG -> List(ru-RU), ADD_PARAM -> List(11111222), LMI_SYS_INVS_NO -> List(), LMI_SYS_TRANS_NO -> List(), LMI_SYS_TRANS_DATE -> List(), LMI_PAYMENT_NO -> List(1))),None,None,None,None,None,false)
		//return redirect(routes.UserController.index());
		
		return redirect("/user?pay=not");
	}

	/**
	 * Paypal payment
	 * 
	 * @param userrequestid
	 * @return
	 */
	public static Result paypalpay(Long userrequestid){
		
		// create a Payment object
		Payment payment = null;
		String guid = UUID.randomUUID().toString().replaceAll("-", "");
		try {
			UserRequests req = UserRequests.findById(userrequestid);
			payment = PaymentHelper.createPayment(req, guid);
		} catch (PayPalRESTException pex) {
			pex.printStackTrace();
		}
		// redirect to PayPal for authorization
		String redirectUrl = null;
		try {
			redirectUrl = getApprovalURL(payment);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		map.put(guid, payment.getId());
		return redirect(redirectUrl);
	}
	
	
	public static Result successepaypal(String guid, String billid, String token, String PayerID){
		
		APIContext apiContext = null;
		String accessToken = null;
		try {
			accessToken = AccessTokenGenerator.getAccessToken();
			apiContext = new APIContext(accessToken);

		} catch (PayPalRESTException e) {
			//req.setAttribute("error", e.getMessage());
		}
		
		Payment payment = new Payment();
		payment.setId(map.get(guid));
		
		PaymentExecution paymentExecution = new PaymentExecution();
		paymentExecution.setPayerId(PayerID);
		
		try {
			payment.execute(apiContext, paymentExecution);
			//req.setAttribute("response", Payment.getLastResponse());
		} catch (PayPalRESTException e) {
			
		}
		
		Bill.updateBillState(Long.parseLong(billid), BillStates.PAYED);
		
		//return redirect(routes.UserController.index());
		return redirect("/user?pay=not");
	}
	
	
	
	public static Result cancelpaypal(String guid, String billid, String token){
		// Order Id used for internal tracking purpose
		//String orderId = (String) request.getParameter("orderId");
		Bill.updateBillState(Long.parseLong(billid), BillStates.UNPAYED);		
				
		//return redirect(routes.UserController.index());
		return redirect("/user?pay=not");
	}
	
	
	
	
	private static String getApprovalURL(Payment payment) throws UnsupportedEncodingException {
		String redirectUrl = null;
		List<Links> links = payment.getLinks();
		for (Links l : links) {
			if (l.getRel().equalsIgnoreCase("approval_url")) {
				redirectUrl = URLDecoder.decode(l.getHref(), AppConstants.UTF_8);
				break;
			}
		}
		return redirectUrl;
	}
}
