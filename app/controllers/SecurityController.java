package controllers;

import static play.data.Form.form;

import com.fasterxml.jackson.databind.node.ObjectNode;

import models.Request;
import models.Role.RoleName;
import models.User;
import models.UserRequests;
import play.data.Form;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import services.SessionService;
import utils.FilesUtils;
import forms.LoginForm;
import forms.PossibleRequestForm;
import forms.RegistrationForm;
import forms.RequestForm;

public class SecurityController extends Controller {

	/**
	 * Handle login form submission.
	 */
	public static Result authenticate() {
		Form<LoginForm> loginForm = form(LoginForm.class).bindFromRequest();
		if (loginForm.hasErrors()) {
			 return forbidden(loginForm.errorsAsJson());
		} else {
			ObjectNode result = Json.newObject();

			session("username", loginForm.get().username);
			
			//remove possible requests
			SessionService.removePossibleRequest();
			result.put("status", "OK");
			return ok(result);
		}
	}

	public static Result registration() {
		
		Form<RegistrationForm> registrationForm = form(RegistrationForm.class).bindFromRequest();
		if (registrationForm.hasErrors()) {
			return forbidden(registrationForm.errorsAsJson());
		}else {
			 User user = User.createUser(registrationForm.get());
			 session("username", user.getUsername());
			 
			 FilesUtils.createUserDirectories(user.getUserId());
			 
			 PossibleRequestForm posibleRequest = SessionService.getPossibleRequest();
			 
			 if (posibleRequest != null) {
		
				 UserRequests userrequest = UserRequests.createUserRequestOfPossibleReq(user, posibleRequest.getDeliveryPrice());
				 for(RequestForm request: posibleRequest.getRequests()){
					 Request.createRequest(request, userrequest, user);
				 }
				 SessionService.removePossibleRequest();
				 
			 }
			return ok();
		}
	}
	
	public static Result logout() {
		session().clear();
		//flash("success", "You've been logged out");
		return redirect(routes.Application.index());
	}
	
	public static boolean isAuthenticatedByRole(String username, String role) {
	    return User.find.where()
	        .eq("role.role", role)
	        .eq("username", username)
	        .findRowCount() > 0;
	}
	
	public static Result redirection(){
		
		User user = User.findByUsername(session("username"));
		if(user != null && user.getRole().getRole().equals(RoleName.ADMIN.toString())){
			return redirect(routes.AdminController.index());
		}else if(user != null && user.getRole().getRole().equals(RoleName.USER.toString())){
			return redirect(routes.UserController.index());
		}
		return redirect(routes.Application.index());
	}
}