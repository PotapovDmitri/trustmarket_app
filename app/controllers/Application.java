package controllers;

import static play.data.Form.form;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;

import javax.imageio.ImageIO;

import models.User;

import org.apache.commons.lang3.RandomStringUtils;
import org.h2.util.StringUtils;

import play.Play;
import play.Routes;
import play.cache.Cache;
import play.data.Form;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Http.Session;
import play.mvc.Result;
import services.SessionService;
import utils.MailUtils;
import utils.MoneyUtils;
import utils.NumericUtils;
import utils.RequestStoreUrl;
import utils.MailUtils.Mails;
import utils.XMLUtils;
import views.html.index;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.google.code.kaptcha.util.Config;

import forms.LoginForm;
import forms.MoreInfoForm;
import forms.PossibleRequestForm;
import forms.RegistrationForm;
import forms.RequestForm;

public class Application extends Controller {

	public static Result index() {

		PossibleRequestForm posibleRequest = SessionService.getPossibleRequest();
		if(posibleRequest == null){
			posibleRequest = new PossibleRequestForm();
		}

		User user = session("username") != "" ? User.findByUsername(session("username")) : new User();
		
		String randomID = RandomStringUtils.randomAlphanumeric(20);
		return ok(index.render(form(LoginForm.class),
				form(RegistrationForm.class), user, posibleRequest, randomID));
	}

	
	public static Result changelang(String lang) {

		if(!StringUtils.isNullOrEmpty(lang)){
			changeLang(lang);
		}
		System.out.println("...........change lang");
		return redirect(routes.Application.index());
	}
	
	
	public static Result getcalculate() {
		ObjectNode result = Json.newObject();
		Session session = Http.Context.current().session();
		final String value = session.get("possiblerequest");
		if (value != null) {
			result.put("status", "OK");
			result.put("result", value);
		} else {
			result.put("status", "FAILURE");
		}
		return ok(result);
	}

	public static Result delposiblerequest() {
		
		ObjectNode result = Json.newObject();
		
		PossibleRequestForm posibleRequest = SessionService.getPossibleRequest();
		if(posibleRequest != null && !posibleRequest.getRequests().isEmpty()){

			Long requestid = Long.valueOf(form().bindFromRequest().get("requestid"));

			for(RequestForm req : posibleRequest.getRequests()){
				if(req.getRequestId() == requestid){
					posibleRequest.getRequests().remove(req);
					break;
				}
			}
			
			result.put("status", "OK");
			if(posibleRequest.getRequests().isEmpty()){
				result.put("requestslist", "empty");
			}else{
				String json = SessionService.setPossibleRequest(posibleRequest);
				result.put("result", json);
			}
		}else{
			result.put("status", "FAILURE");
		}
		
		return ok(result);
	}

	public static Result getposiblerequest() {
		ObjectNode result = Json.newObject();
		PossibleRequestForm posibleRequest = SessionService.getPossibleRequest();
		if(posibleRequest != null){
			
			Long requestid = Long.valueOf(form().bindFromRequest().get("requestid"));
			
			RequestForm request = null;
			for(RequestForm req : posibleRequest.getRequests()){
				if(req.getRequestId() == requestid){
					request = req;
					break;
				}
			}
			
			if(request != null){
				result.put("status", "OK");
				result.put("result", Json.toJson(request));
				System.out.println("JSON : " +Json.toJson(request));
			}else{
				result.put("status", "FAILURE");
			}
		}else{
			result.put("status", "FAILURE");
		}
		
		
		return ok(result);
	}

	public static Result setdeliveryprice() {
		double deliveryprice = NumericUtils.tryToParseDouble(form()
				.bindFromRequest().get("deliveryprice"), 0.0);

		ObjectNode result = Json.newObject();
		
		PossibleRequestForm posibleRequest = SessionService.getPossibleRequest();

		if(posibleRequest != null){
			posibleRequest.setDeliveryPrice(deliveryprice);

			SessionService.setPossibleRequest(posibleRequest);

			result.put("status", "OK");
			result.put("deliveryprice",
					MoneyUtils.priceToString(posibleRequest.getDeliveryPrice()));
			result.put("totalprice",
					MoneyUtils.priceToString(posibleRequest.getTotalPrice()));
		} else {
			result.put("status", "FAILURE");
		}
		
		return ok(result);
	}

	public static Result calculate() {
		Form<RequestForm> form = form(RequestForm.class).bindFromRequest();
		 ObjectNode result = Json.newObject();

		if (form.hasErrors()) {
			return forbidden(form.errorsAsJson());
		} else {
			
			
			RequestForm request = form.get();

			PossibleRequestForm posibleRequest = SessionService.getPossibleRequest();

			String storeListValidation = RequestStoreUrl.validatePosibleRequestStoreLink(request.getRequestId(), request.getStorelink(), posibleRequest);
			if(utils.StringUtils.notEmpty(storeListValidation)){
				return badRequest("{\"errors\":[\"storelink;"+storeListValidation+"\"]}");
			}

			String json = "";
			String action = "";
			if (posibleRequest == null) {
				posibleRequest = new PossibleRequestForm();
				posibleRequest.addRequest(request);

				action = "added";
				
			} else {
				
				if(request.getRequestId() != null && request.getRequestId() > 0){
					posibleRequest.updateRequest(request);
					action = "updated";
				}else{
					posibleRequest.addRequest(request);
					action = "added";
				}
			}
			json = SessionService.setPossibleRequest(posibleRequest);

			result.put("request_action", action);
			result.put("result", json);
			result.put("status", "OK");

			return ok(result);
		}
	}
	
	public static Result getconditional(){
		ObjectNode result = Json.newObject();
		
		String content = XMLUtils.getCondition(lang().code());
		
		if(utils.StringUtils.notEmpty(content)){
			result.put("result", content);
			result.put("status", "OK");
		}else{
			result.put("status", "FAILURE");
		}
		return ok(result);
	}
	
	public static Result captcha(String randomID){
	    DefaultKaptcha captchaPro=new DefaultKaptcha();
	    captchaPro.setConfig(new Config(new Properties()));
	    String text=captchaPro.createText();
	    
	    Cache.set(randomID, text);
	    
	    BufferedImage img = captchaPro.createImage(text);
	    
	    ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    try{
	        ImageIO.write(img, "jpg", baos);
	        baos.flush();
	    }catch(IOException e){
	        System.err.println(e.getMessage());
	    }
	    return ok(baos.toByteArray()).as("image/jpg");
	}
	
	public static Result sendmoreinfoemail() {
		
		ObjectNode result = Json.newObject();
		
		Form<MoreInfoForm> form = form(MoreInfoForm.class).bindFromRequest();
		
		if (!form.hasErrors()) {
			MoreInfoForm info = form.get();
		    
			Map<String, String> mail = MailUtils.getMailContent(Mails.MAIL_MOREINFO);
			
			String content = mail.get("content");
			
			content = content.replace("<content>", info.getMessage());
			content = content.replace("<email>", info.getEmail());
			
			String adminEmail = Play.application().configuration().getString("smtp.user");
			
			MailUtils.sendEmail(content, mail.get("subject"), adminEmail);
			
			result.put("status", "OK");
		}else{
			return forbidden(form.errorsAsJson());
		}
		
		return ok(result);
    }

	public static Result javascriptRoutes() {
		response().setContentType("text/javascript");
		return ok(Routes
				.javascriptRouter("jsRoutes",
						controllers.routes.javascript.SecurityController
								.authenticate(),
						controllers.routes.javascript.SecurityController
								.registration(),
						controllers.routes.javascript.UserController
								.deleterequest(),
						controllers.routes.javascript.UserController
								.getrequest(),
						controllers.routes.javascript.UserController
								.editdeliveryprice(),
						controllers.routes.javascript.UserController
								.sendrequest(),
						controllers.routes.javascript.UserController
								.getcurrentuser(),
						controllers.routes.javascript.UserController
								.updatecurrentuser(),
						controllers.routes.javascript.UserController
								.createrequest(),
						controllers.routes.javascript.UserController
								.deleterequestfile(),
						controllers.routes.javascript.Application.calculate(),
						controllers.routes.javascript.Application.getconditional(),
						controllers.routes.javascript.Application.getcalculate(),
						controllers.routes.javascript.Application.setdeliveryprice(),
						controllers.routes.javascript.Application.delposiblerequest(),
						controllers.routes.javascript.Application.getposiblerequest(),
						controllers.routes.javascript.Application.sendmoreinfoemail(),
						controllers.routes.javascript.AdminController.createbill(),
						controllers.routes.javascript.AdminController.markpayed(),
						controllers.routes.javascript.AdminController.markcanceled()
				));
	}

}
