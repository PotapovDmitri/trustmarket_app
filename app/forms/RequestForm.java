package forms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import play.data.validation.ValidationError;
import play.i18n.Messages;
import utils.StringUtils;


public class RequestForm implements Serializable{

	public String type;

	public String storelink;
	
	public String name;

	public String size;
	
	public String article;

	public Double price;
	
	public Double totalsumma;

	public Integer count;

	public String color;

	public String description;
	
	public String packagecode;
	
	public Long userRequestId;
	
	public Long requestId;

	public Map<String, List<ValidationError>> validate() {
		 Map<String,List<ValidationError>> errorList = new HashMap<String, List<ValidationError>>();
		 List<ValidationError> requiredErrorList = new ArrayList<ValidationError>();
		 errorList.put("errors", requiredErrorList);
		 
		if(StringUtils.isEmpty(type)){
			requiredErrorList.add(new ValidationError("required", "type;"+Messages.get("type.empty")));
		}
		if(price == null || price == 0){
			requiredErrorList.add(new ValidationError("required", "price;"+Messages.get("price.empty")));
		}
		if("1".equals(type)){
			if(StringUtils.isEmpty(name)){
				requiredErrorList.add(new ValidationError("required", "name;"+Messages.get("name.empty")));
			}
			if(StringUtils.isEmpty(storelink)){
				requiredErrorList.add(new ValidationError("required", "storelink;"+Messages.get("storelink.empty")));
			}
			if(count == null || count == 0){
				requiredErrorList.add(new ValidationError("required", "count;"+Messages.get("count.empty")));
			}
			
		}else if("2".equals(type)){
			if(StringUtils.isEmpty(packagecode)){
				requiredErrorList.add(new ValidationError("required", "packagecode;"+Messages.get("packagecode.empty")));
			}
		}
		
		
		return (requiredErrorList.isEmpty() ?  null : errorList);
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStorelink() {
		return storelink;
	}

	public void setStorelink(String storelink) {
		this.storelink = storelink;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getArticle() {
		return article;
	}

	public void setArticle(String article) {
		this.article = article;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPackagecode() {
		return packagecode;
	}

	public void setPackagecode(String packagecode) {
		this.packagecode = packagecode;
	}

	public Long getUserRequestId() {
		return userRequestId;
	}

	public void setUserRequestId(Long userRequestId) {
		this.userRequestId = userRequestId;
	}

	public Long getRequestId() {
		return requestId;
	}

	public void setRequestId(Long requestId) {
		this.requestId = requestId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getTotalsumma() {
		return totalsumma;
	}

	public void setTotalsumma(Double totalsumma) {
		this.totalsumma = totalsumma;
	}
}