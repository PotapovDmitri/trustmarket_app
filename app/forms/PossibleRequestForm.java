package forms;

import java.util.ArrayList;
import java.util.List;

import utils.NumericUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

public class PossibleRequestForm {

	public Double deliveryPrice;
	public Double commissionPrice;
	public Double totalPrice;
	public Double totalRequestsPrices;
	
	public List<RequestForm> requests;
	
	public Double getTotalRequestsPrices(){
		Double prices = 0.0; 
    	if(requests.size() > 0){
    		for(RequestForm request: this.requests){
    			prices = prices + (request.getPrice() * request.getCount());
    		}
    	}
    	return NumericUtils.round(prices, 2);
    }
    public void setTotalRequestsPrices(Double totalRequestsPrices) {
		this.totalRequestsPrices = totalRequestsPrices;
	}
    public Double getCommissionPrice(){
		Double requestsprices = 0.0; 
    	if(requests.size() > 0){
    		for(RequestForm request: this.requests){
    			requestsprices = requestsprices + (request.getPrice() * request.getCount());
    		}
    	}
    	
    	Double commission = requestsprices * 0.1;
    	if(10 > commission){
    		commission = 10.00;
    	}
    	return NumericUtils.round(commission, 2);
    }

	public void setCommissionPrice(Double commissionPrice) {
		this.commissionPrice = commissionPrice;
	}
    
    public Double getTotalPrice(){
    	return NumericUtils.round(getTotalRequestsPrices() +  getCommissionPrice() + getDeliveryPrice(), 2);
    }
    public void setTotalPrice(Double totalPrice ){
    	this.totalPrice = totalPrice;
    }
    
	public Double getDeliveryPrice() {
		return deliveryPrice != null ? deliveryPrice : 0.00;
	}
	public void setDeliveryPrice(Double deliveryPrice) {
		this.deliveryPrice = deliveryPrice;
	}

	public List<RequestForm> getRequests() {
		return requests;
	}

	public void setRequests(List<RequestForm> requests) {
		this.requests = requests;
	}
	

	@JsonIgnoreProperties
	public void addRequest(RequestForm form){
		requests = (requests == null ?  new ArrayList<RequestForm>() : requests);
		if(requests.isEmpty()){
			form.setRequestId(1L);
		}else{
			form.setRequestId(requests.size() + 1L);
		}
		requests.add(form);
	}
	
	@JsonIgnoreProperties
	public void updateRequest(RequestForm form){
		if(!requests.isEmpty()){
			
			for(RequestForm req : requests){
				if(req.getRequestId() == form.getRequestId()){
					
					req.setStorelink(form.getStorelink());
					req.setName(form.getName());
					req.setSize(form.getSize());
					req.setArticle(form.getArticle());
					req.setPrice(form.getPrice());
					req.setColor(form.getColor());
					req.setCount(form.getCount());
					req.setDescription(form.getDescription());
					break;
				}
			}
			
		}
	}
}
