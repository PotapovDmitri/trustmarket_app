package forms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import play.data.validation.ValidationError;
import play.i18n.Messages;

public class BillForm {

	public Double totalsumma;
	public Double productsumma;
	public Double deliverysumma;
	public Double servicesumma;
	public Long userrequestid;
	public String type;

	public Map<String, List<ValidationError>> validate() {
		
		Map<String, List<ValidationError>> errorList = new HashMap<String, List<ValidationError>>();
		List<ValidationError> requiredErrorList = new ArrayList<ValidationError>();
		errorList.put("errors", requiredErrorList);

		if (productsumma == null) {
			requiredErrorList.add(new ValidationError("required", "productsumma;"+Messages.get("invalid.productsumma")));
		}
		if (totalsumma == null) {
			requiredErrorList.add(new ValidationError("required", "totalsumma;"+Messages.get("invalid.totalsumma")));
		}
		if (servicesumma == null) {
			requiredErrorList.add(new ValidationError("required", "servicesumma;"+Messages.get("invalid.servicesumma")));
		}
		if (userrequestid == null) {
			requiredErrorList.add(new ValidationError("required", "userrequestid;"+Messages.get("invalid.userrequestid")));
		}
		return (requiredErrorList.isEmpty() ? null : errorList);
	}

	public Double getTotalsumma() {
		return totalsumma;
	}

	public void setTotalsumma(Double totalsumma) {
		this.totalsumma = totalsumma;
	}

	public Double getDeliverysumma() {
		return deliverysumma;
	}

	public void setDeliverysumma(Double deliverysumma) {
		this.deliverysumma = deliverysumma;
	}

	public Double getServicesumma() {
		return servicesumma;
	}

	public void setServicesumma(Double servicesumma) {
		this.servicesumma = servicesumma;
	}

	public Long getUserrequestid() {
		return userrequestid;
	}

	public void setUserrequestid(Long userrequestid) {
		this.userrequestid = userrequestid;
	}

	public Double getProductsumma() {
		return productsumma;
	}

	public void setProductsumma(Double productsumma) {
		this.productsumma = productsumma;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	
}