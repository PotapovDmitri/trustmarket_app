package forms;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import models.User;
import play.data.format.Formats;
import play.data.validation.ValidationError;
import play.i18n.Messages;
import utils.StringUtils;


public class RegistrationForm {
	public String username;
	public String password;
	public String password2;
	public String firstname;
	public String lastname;

	@Formats.DateTime(pattern = "dd.MM.yyyy")
	public Date birthdate;
	
	public String passportnr;
	public String email;

	public Map<String, List<ValidationError>> validate() {
		 Map<String,List<ValidationError>> errorList = new HashMap<String, List<ValidationError>>();
		 List<ValidationError> requiredErrorList = new ArrayList<ValidationError>();
		 errorList.put("errors", requiredErrorList);
		 
		if(StringUtils.isEmpty(username)){
			requiredErrorList.add(new ValidationError("required", "username;"+Messages.get("username.empty")));
		}else if(User.findByUsername(username) != null){
			requiredErrorList.add(new ValidationError("required", "username;"+Messages.get("username.exists")));
		}
		if(StringUtils.isEmpty(firstname)){
			requiredErrorList.add(new ValidationError("required", "firstname;"+Messages.get("firstname.empty")));
		}
		if(StringUtils.isEmpty(lastname)){
			requiredErrorList.add(new ValidationError("required", "lastname;"+Messages.get("lastname.empty")));
		}
		if(StringUtils.isEmpty(passportnr)){
			requiredErrorList.add(new ValidationError("required", "passportnr;"+Messages.get("passportnr.empty")));
		}
		if(StringUtils.isEmpty(email)){
			requiredErrorList.add(new ValidationError("required", "email;"+Messages.get("email.empty")));
		}else if(User.findByEmail(email) != null){
			requiredErrorList.add(new ValidationError("required", "email;"+Messages.get("email.exists")));
		}
		if(StringUtils.isEmpty(password) || StringUtils.isEmpty(password2)){
			requiredErrorList.add(new ValidationError("required", "password;"+Messages.get("password.empty")));
		}else if(!password.equals(password2)){
			requiredErrorList.add(new ValidationError("required", "password;"+Messages.get("password.noequal")));
		}
		
		
		return (requiredErrorList.isEmpty() ?  null : errorList);
	}
}
