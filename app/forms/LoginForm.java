package forms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import play.data.validation.ValidationError;
import play.i18n.Messages;
import services.PasswordService;
import models.User;

public class LoginForm {

	public String username;
	public String password;

	public Map<String, List<ValidationError>> validate() {

		Map<String, List<ValidationError>> errorList = new HashMap<String, List<ValidationError>>();
		List<ValidationError> requiredErrorList = new ArrayList<ValidationError>();
		errorList.put("errors", requiredErrorList);

		User user = User.findByUsername(username);
		if (user == null) {
			requiredErrorList.add(new ValidationError("required",
					"username;"+Messages.get("invalid.username")));
		}else if (!PasswordService.checkPassword(password, user.getPassword())) {
			requiredErrorList.add(new ValidationError("required",
					"username;"+Messages.get("invalid.password")));
		}
		return (requiredErrorList.isEmpty() ? null : errorList);
	}
}
