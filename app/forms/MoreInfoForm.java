package forms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import play.cache.Cache;
import play.data.validation.ValidationError;
import play.i18n.Messages;
import utils.StringUtils;
import utils.Validations;

public class MoreInfoForm {
	
	public String email;
	public String name;
	public String message;
	public String code;
	public String randomID;
	
	public Map<String, List<ValidationError>> validate() {
		 Map<String,List<ValidationError>> errorList = new HashMap<String, List<ValidationError>>();
		 List<ValidationError> requiredErrorList = new ArrayList<ValidationError>();
		 errorList.put("errors", requiredErrorList);
		 
		 System.out.println("................cone:"+code);
		 System.out.println(randomID+"................randomID:"+Cache.get(randomID));
		 if(StringUtils.isEmpty(code)){
			 requiredErrorList.add(new ValidationError("required", "code;"+Messages.get("capchacode.empty")));
		 }else if(!code.equals(Cache.get(randomID))){
			 
			 requiredErrorList.add(new ValidationError("required", "code;"+Messages.get("capchacode.wrong")));
		 }
		 
		 if(StringUtils.isEmpty(name)){
			 requiredErrorList.add(new ValidationError("required", "name;"+Messages.get("name.empty")));
		 }
		 
		 if(StringUtils.isEmpty(email)){
			 requiredErrorList.add(new ValidationError("required", "email;"+Messages.get("email.empty")));
		 }else if(!Validations.validateEmailAddress(email)){
			 requiredErrorList.add(new ValidationError("required", "email;"+Messages.get("email.wrong")));
		 }
		 
		 if(StringUtils.isEmpty(message)){
			 requiredErrorList.add(new ValidationError("required", "message;"+Messages.get("message.empty")));
		 }
		 
		 return (requiredErrorList.isEmpty() ?  null : errorList);
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getRandomID() {
		return randomID;
	}

	public void setRandomID(String randomID) {
		this.randomID = randomID;
	}
	
	
}
