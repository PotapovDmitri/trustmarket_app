package forms;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import play.data.format.Formats;
import play.data.validation.ValidationError;
import utils.StringUtils;

public class UserForm {

	public Integer userid;
	public String username;
	public String firstname;
	public String lastname;

	@Formats.DateTime(pattern = "dd.MM.yyyy")
	public Date birthdate;
	
	public String passportnr;
	public String email;
	
	public Map<String, List<ValidationError>> validate() {
		 Map<String,List<ValidationError>> errorList = new HashMap<String, List<ValidationError>>();
		 List<ValidationError> requiredErrorList = new ArrayList<ValidationError>();
		 errorList.put("errors", requiredErrorList);
		 
		if(StringUtils.isEmpty(firstname)){
			requiredErrorList.add(new ValidationError("required", "firstname;firstname.empty"));
		}
		if(StringUtils.isEmpty(lastname)){
			requiredErrorList.add(new ValidationError("required", "lastname;lastname.empty"));
		}
		if(StringUtils.isEmpty(email)){
			requiredErrorList.add(new ValidationError("required", "email;email.empty"));
		}
		if(StringUtils.isEmpty(passportnr)){
			requiredErrorList.add(new ValidationError("required", "passportnr;passportnr.empty"));
		}
		
		return (requiredErrorList.isEmpty() ?  null : errorList);
	}

	public Integer getUserid() {
		return userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public String getPassportnr() {
		return passportnr;
	}

	public void setPassportnr(String passportnr) {
		this.passportnr = passportnr;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
}