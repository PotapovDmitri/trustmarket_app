package forms;

public class MessageForm {

	public String message;
	public Long userrequestid;
	public Long origmsgid;

	public String validate() {
		if(message == null){
			return "Empty message";
		}
		if(origmsgid == null){
			return "Replay isn't undefined";
		}
		if(userrequestid == null){
			return "Request isn't undefined";
		}
		return null;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Long getUserrequestid() {
		return userrequestid;
	}

	public void setUserrequestid(Long userrequestid) {
		this.userrequestid = userrequestid;
	}

	public Long getOrigmsgid() {
		return origmsgid;
	}

	public void setOrigmsgid(Long origmsgid) {
		this.origmsgid = origmsgid;
	}


}