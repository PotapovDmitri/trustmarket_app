package services;

import java.io.IOException;

import play.libs.Json;
import play.mvc.Http;
import play.mvc.Http.Session;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import forms.PossibleRequestForm;

public class SessionService {

	public static PossibleRequestForm getPossibleRequest() {
		Session session = Http.Context.current().session();

		final String json = session.get("possiblerequest");
		if (json != null) {
			ObjectMapper mapper = new ObjectMapper();
			JsonNode obj = null;
			try {
				obj = mapper.readValue(json, JsonNode.class);
			} catch (JsonParseException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return Json.fromJson(obj, PossibleRequestForm.class);
		}
		return null;
	}
	
	public static void removePossibleRequest() {
		Session session = Http.Context.current().session();
		session.remove("possiblerequest");
	}
	
	public static String setPossibleRequest(PossibleRequestForm posibleRequest) {
		String json = "";
		if(posibleRequest != null){
			JsonNode jsonnode = Json.toJson(posibleRequest);
			Session session = Http.Context.current().session();
			json = jsonnode.toString();
			session.put("possiblerequest", json);
			
			
		}
		
		System.out.println("SESSION JSON: " +json);
		
		return json;
	}

}