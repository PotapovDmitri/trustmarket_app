package services.payments.messages;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import services.payments.IBanksAuthRequest;
import services.payments.IBanksUtils;
import utils.AppConstants;


public class IBanksMessage1012 extends IBanksMessage implements IBanksAuthRequest {
	
	protected String VK_STAMP;
	protected String VK_AMOUNT;
	protected String VK_CURR;
	protected String VK_REF;
	protected String VK_MSG;

	protected String VK_LANG;
	protected String VK_DATETIME;
	protected String VK_RETURN;
	protected String VK_CANCEL;
	protected String VK_ENCODING;

	public IBanksMessage1012(String VK_STAMP, String VK_AMOUNT, String VK_REF, String VK_MSG) {
		this.VK_SERVICE = "1012";
		this.VK_STAMP = VK_STAMP;
		this.VK_AMOUNT = VK_AMOUNT;
		this.VK_CURR = AppConstants.BANK_CURR;
		this.VK_REF = VK_REF;
		this.VK_MSG = VK_MSG;
		this.VK_LANG = AppConstants.BANK_LANG;
		this.VK_ENCODING = AppConstants.BANK_DEFAULT_ENCODING;
		
		TimeZone tz = TimeZone.getTimeZone("UTC");
	    DateFormat df = new SimpleDateFormat(AppConstants.BANK_DATETIME_FORMAT);
	    df.setTimeZone(tz);
	    this.VK_DATETIME = df.format(new Date());
	}

	public IBanksMessage1012() {
		this.VK_SERVICE = "1012";
		this.VK_CURR = AppConstants.BANK_CURR;
		this.VK_ENCODING = AppConstants.BANK_DEFAULT_ENCODING;
		this.VK_LANG = AppConstants.BANK_LANG;

		TimeZone tz = TimeZone.getTimeZone("UTC");
	    DateFormat df = new SimpleDateFormat(AppConstants.BANK_DATETIME_FORMAT);
	    df.setTimeZone(tz);
	    this.VK_DATETIME = df.format(new Date());
	}

	public String getMessage() {
		StringBuffer msg = new StringBuffer(fmtElement(this.VK_SERVICE));
		msg.append(fmtElement(this.VK_VERSION));
		msg.append(fmtElement(this.VK_SND_ID));
		msg.append(fmtElement(this.VK_STAMP));
		msg.append(fmtElement(this.VK_AMOUNT));
		msg.append(fmtElement(this.VK_CURR));
		msg.append(fmtElement(this.VK_REF));
		msg.append(fmtElement(this.VK_MSG));
		msg.append(fmtElement(this.VK_RETURN));
		msg.append(fmtElement(this.VK_CANCEL));
		msg.append(fmtElement(this.VK_DATETIME));

		return msg.toString();
	}

	public String toHTML() {
		StringBuffer buf = new StringBuffer();
		buf.append(super.toHTML());
		buf.append(IBanksUtils.toHTMLHidden("VK_STAMP", getVK_STAMP()));
		buf.append(IBanksUtils.toHTMLHidden("VK_AMOUNT", getVK_AMOUNT()));
		buf.append(IBanksUtils.toHTMLHidden("VK_CURR", getVK_CURR()));
		buf.append(IBanksUtils.toHTMLHidden("VK_REF", getVK_REF()));
		buf.append(IBanksUtils.toHTMLHidden("VK_MSG", getVK_MSG()));
		
		buf.append(IBanksUtils.toHTMLHidden("VK_RETURN", getVK_RETURN()));
		buf.append(IBanksUtils.toHTMLHidden("VK_CANCEL", getVK_CANCEL()));
		buf.append(IBanksUtils.toHTMLHidden("VK_LANG", getVK_LANG()));
		buf.append(IBanksUtils.toHTMLHidden("VK_DATETIME", getVK_DATETIME()));
		buf.append(IBanksUtils.toHTMLHidden("VK_ENCODING", getVK_ENCODING()));
		return buf.toString();
	}

	public String getVK_LANG() {
		return VK_LANG;
	}

	public void setVK_LANG(String vK_LANG) {
		VK_LANG = vK_LANG;
	}

	public String getVK_STAMP() {
		return VK_STAMP;
	}

	public void setVK_STAMP(String vK_STAMP) {
		VK_STAMP = vK_STAMP;
	}

	public String getVK_CURR() {
		return VK_CURR;
	}

	public void setVK_CURR(String vK_CURR) {
		VK_CURR = vK_CURR;
	}

	public String getVK_AMOUNT() {
		return VK_AMOUNT;
	}

	public void setVK_AMOUNT(String vK_AMOUNT) {
		VK_AMOUNT = vK_AMOUNT;
	}

	public String getVK_REF() {
		return VK_REF;
	}

	public void setVK_REF(String vK_REF) {
		VK_REF = vK_REF;
	}

	public String getVK_MSG() {
		return VK_MSG;
	}

	public void setVK_MSG(String vK_MSG) {
		VK_MSG = vK_MSG;
	}

	public String getVK_RETURN() {
		return this.VK_RETURN;
	}

	public void setVK_RETURN(String VK_RETURN) {
		this.VK_RETURN = VK_RETURN;
	}

	public String getVK_CANCEL() {
		return this.VK_CANCEL;
	}

	public void setVK_CANCEL(String VK_CANCEL) {
		this.VK_CANCEL = VK_CANCEL;
	}

	
	public String getVK_DATETIME() {
		return VK_DATETIME;
	}

	public void setVK_DATETIME(String vK_DATETIME) {
		VK_DATETIME = vK_DATETIME;
	}

	public String getVK_ENCODING() {
		return VK_ENCODING;
	}

	public void setVK_ENCODING(String vK_ENCODING) {
		VK_ENCODING = vK_ENCODING;
	}

	public String toString() {
		return getMessage();
	}
}
