package services.payments.messages;

import java.util.Date;

import services.payments.IBanksAuthRequest;
import services.payments.IBanksUtils;

public class IBanksMessage4001 extends IBanksMessage implements IBanksAuthRequest {
	
  protected String VK_REPLY = "3002";
  protected String VK_RETURN;
  protected String VK_DATE;
  protected String VK_TIME;
  
  public IBanksMessage4001()
  {
    this.VK_SERVICE = "4001";
  }
  
  public String getMessage()
  {
    StringBuffer msg = new StringBuffer(fmtElement(this.VK_SERVICE));
    msg.append(fmtElement(this.VK_VERSION));
    msg.append(fmtElement(this.VK_SND_ID));
    msg.append(fmtElement(this.VK_REPLY));
    msg.append(fmtElement(this.VK_RETURN));
    msg.append(fmtElement(this.VK_DATE));
    msg.append(fmtElement(this.VK_TIME));
    
    return msg.toString();
  }
  
  public String toHTML()
  {
    StringBuffer buf = new StringBuffer();
    buf.append(super.toHTML());
    buf.append(IBanksUtils.toHTMLHidden("VK_REPLY", getVK_REPLY()));
    buf.append(IBanksUtils.toHTMLHidden("VK_RETURN", getVK_RETURN()));
    buf.append(IBanksUtils.toHTMLHidden("VK_DATE", getVK_DATE()));
    buf.append(IBanksUtils.toHTMLHidden("VK_TIME", getVK_TIME()));
    return buf.toString();
  }
  
  public String getVK_REPLY()
  {
    return this.VK_REPLY;
  }
  
  public void setVK_REPLY(String VK_REPLY)
  {
    this.VK_REPLY = VK_REPLY;
  }
  
  public String getVK_RETURN()
  {
    return this.VK_RETURN;
  }
  
  public void setVK_RETURN(String VK_RETURN)
  {
    this.VK_RETURN = VK_RETURN;
  }
  
  public String getVK_DATE()
  {
    return this.VK_DATE;
  }
  
  public void setVK_DATE(String VK_DATE)
  {
    this.VK_DATE = VK_DATE;
  }
  
  public void setVK_DATE(Date VK_DATE)
  {
    this.VK_DATE = fmtDate(VK_DATE);
  }
  
  public String getVK_TIME()
  {
    return this.VK_TIME;
  }
  
  public void setVK_TIME(String VK_TIME)
  {
    this.VK_TIME = VK_TIME;
  }
  
  public void setVK_TIME(Date VK_TIME)
  {
    this.VK_TIME = fmtTime(VK_TIME);
  }
  
  public String toString()
  {
    return getMessage();
  }
}
