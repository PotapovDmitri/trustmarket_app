package services.payments.messages;

import services.payments.IBanksAuthResponse;

public class IBanksMessage3003 extends IBanksMessage implements	IBanksAuthResponse {
	
	public static final String DEFAULT_SERVICE = "3003";
	protected String VK_REC_ID;
	protected String VK_NONCE;
	protected String VK_INFO;

	public IBanksMessage3003() {
		this.VK_SERVICE = "3003";
	}

	public String getMessage() {
		StringBuffer msg = new StringBuffer(fmtElement(this.VK_SERVICE));
		msg.append(fmtElement(this.VK_VERSION));
		msg.append(fmtElement(this.VK_SND_ID));
		msg.append(fmtElement(this.VK_REC_ID));
		msg.append(fmtElement(this.VK_NONCE));
		msg.append(fmtElement(this.VK_INFO));

		return msg.toString();
	}

	public String getVK_REC_ID() {
		return this.VK_REC_ID;
	}

	public void setVK_REC_ID(String VK_REC_ID) {
		this.VK_REC_ID = VK_REC_ID;
	}

	public String getVK_NONCE() {
		return this.VK_NONCE;
	}

	public void setVK_NONCE(String VK_NONCE) {
		this.VK_NONCE = VK_NONCE;
	}

	public String getVK_INFO() {
		return this.VK_INFO;
	}

	public void setVK_INFO(String VK_INFO) {
		this.VK_INFO = VK_INFO;
	}

	public void prepare() {
	}

	public String toString() {
		return getMessage();
	}
}
