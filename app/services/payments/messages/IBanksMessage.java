package services.payments.messages;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import services.payments.IBanksUtils;

public abstract class IBanksMessage implements Serializable {
	
  public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy");
  public static final SimpleDateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm:ss");
  public static final SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
  
  protected String VK_VERSION = "008";
  protected String VK_MAC;
  protected String VK_SERVICE;
  protected String VK_SND_ID;
  
  public abstract String getMessage();
  
  public String toHTML()
  {
    StringBuffer buf = new StringBuffer();
    buf.append(IBanksUtils.toHTMLHidden("VK_SERVICE", getVK_SERVICE()));
    buf.append(IBanksUtils.toHTMLHidden("VK_VERSION", getVK_VERSION()));
    buf.append(IBanksUtils.toHTMLHidden("VK_SND_ID", getVK_SND_ID()));
    buf.append(IBanksUtils.toHTMLHidden("VK_MAC", getVK_MAC()));
    return buf.toString();
  }
  
  protected static String fmtLength(String data)
  {
    int length = data == null ? 0 : data.length();
    if (length < 10) {
      return "00" + length;
    }
    if (length < 100) {
      return "0" + length;
    }
    return "" + length;
  }
  
  protected static String fmtElement(String data)
  {
    if (data == null) {
      data = "";
    }
    return fmtLength(data) + data;
  }
  
  protected static String fmtDate(Date d)
  {
    return DATE_FORMAT.format(d);
  }
  
  protected static String fmtTime(Date d)
  {
    return TIME_FORMAT.format(d);
  }
  
  public String getVK_VERSION()
  {
    return this.VK_VERSION;
  }
  
  public void setVK_VERSION(String vK_VERSION) {
	VK_VERSION = vK_VERSION;
  }

  public String getVK_MAC() {
    return this.VK_MAC;
  }
  
  public void setVK_MAC(String VK_MAC)
  {
    this.VK_MAC = VK_MAC;
  }
  
  public String getVK_SERVICE()
  {
    return this.VK_SERVICE;
  }
  
  public void setVK_SERVICE(String VK_SERVICE)
  {
    this.VK_SERVICE = VK_SERVICE;
  }
  
  public String getVK_SND_ID()
  {
    return this.VK_SND_ID;
  }
  
  public void setVK_SND_ID(String VK_SND_ID)
  {
    this.VK_SND_ID = VK_SND_ID;
  }
}
