package services.payments.messages;

import services.payments.IBanksAuthRequest;


public class IBanksMessage1101 extends IBanksMessage implements IBanksAuthRequest {
	
	protected String VK_REC_ID;
	protected String VK_STAMP;
	protected String VK_T_NO;
	protected String VK_AMOUNT;
	protected String VK_CURR;
	protected String VK_REC_ACC;
	protected String VK_REC_NAME;
	protected String VK_SND_ACC;
	protected String VK_SND_NAME;
	protected String VK_REF;
	protected String VK_MSG;
	protected String VK_T_DATE;
	protected String VK_LANG;

	public IBanksMessage1101() {
		this.VK_SERVICE = "1101";
		this.VK_LANG = "EST";
		this.VK_REF = "";
		this.VK_CURR = "EUR";
	}
	
	public IBanksMessage1101(String VK_REC_ID, String VK_STAMP, String VK_T_NO, String VK_AMOUNT, String VK_REC_ACC,
			String VK_REC_NAME, String VK_SND_ACC, String VK_SND_NAME, String VK_REF,  String VK_MSG, String VK_T_DATE) {
		this.VK_SERVICE = "1101";
		this.VK_REC_ID = VK_REC_ID;
		this.VK_STAMP = VK_STAMP;
		this.VK_T_NO = VK_T_NO;
		this.VK_AMOUNT = VK_AMOUNT;
		this.VK_CURR = "EUR";
		this.VK_REC_ACC = VK_REC_ACC;
		this.VK_REC_NAME = VK_REC_NAME;
		this.VK_SND_ACC = VK_SND_ACC;
		this.VK_SND_NAME = VK_SND_NAME;
		this.VK_REF = VK_REF;
		this.VK_MSG = VK_MSG;
		this.VK_T_DATE = VK_T_DATE;
		this.VK_LANG = "EST";
	}

	public String getMessage() {
		StringBuffer msg = new StringBuffer(fmtElement(this.VK_SERVICE));
		msg.append(fmtElement(this.VK_VERSION));
		msg.append(fmtElement(this.VK_SND_ID));
		msg.append(fmtElement(this.VK_REC_ID));
		msg.append(fmtElement(this.VK_STAMP));
		msg.append(fmtElement(this.VK_T_NO));
		msg.append(fmtElement(this.VK_AMOUNT));
		msg.append(fmtElement(this.VK_CURR));
		msg.append(fmtElement(this.VK_REC_ACC));
		msg.append(fmtElement(this.VK_REC_NAME));
		msg.append(fmtElement(this.VK_SND_ACC));
		msg.append(fmtElement(this.VK_SND_NAME));
		msg.append(fmtElement(this.VK_REF));
		msg.append(fmtElement(this.VK_MSG));
		msg.append(fmtElement(this.VK_T_DATE));

		return msg.toString();
	}

	public String toHTML() {
		StringBuffer buf = new StringBuffer();
		
		return buf.toString();
	}
	
	public String getVK_REC_ID() {
		return VK_REC_ID;
	}

	public void setVK_REC_ID(String vK_REC_ID) {
		VK_REC_ID = vK_REC_ID;
	}

	public String getVK_STAMP() {
		return VK_STAMP;
	}

	public void setVK_STAMP(String vK_STAMP) {
		VK_STAMP = vK_STAMP;
	}

	public String getVK_T_NO() {
		return VK_T_NO;
	}

	public void setVK_T_NO(String vK_T_NO) {
		VK_T_NO = vK_T_NO;
	}

	public String getVK_AMOUNT() {
		return VK_AMOUNT;
	}

	public void setVK_AMOUNT(String vK_AMOUNT) {
		VK_AMOUNT = vK_AMOUNT;
	}

	public String getVK_CURR() {
		return VK_CURR;
	}

	public void setVK_CURR(String vK_CURR) {
		VK_CURR = vK_CURR;
	}

	public String getVK_REC_ACC() {
		return VK_REC_ACC;
	}

	public void setVK_REC_ACC(String vK_REC_ACC) {
		VK_REC_ACC = vK_REC_ACC;
	}

	public String getVK_REC_NAME() {
		return VK_REC_NAME;
	}

	public void setVK_REC_NAME(String vK_REC_NAME) {
		VK_REC_NAME = vK_REC_NAME;
	}

	public String getVK_SND_ACC() {
		return VK_SND_ACC;
	}

	public void setVK_SND_ACC(String vK_SND_ACC) {
		VK_SND_ACC = vK_SND_ACC;
	}

	public String getVK_SND_NAME() {
		return VK_SND_NAME;
	}

	public void setVK_SND_NAME(String vK_SND_NAME) {
		VK_SND_NAME = vK_SND_NAME;
	}

	public String getVK_REF() {
		return VK_REF;
	}

	public void setVK_REF(String vK_REF) {
		VK_REF = vK_REF;
	}

	public String getVK_MSG() {
		return VK_MSG;
	}

	public void setVK_MSG(String vK_MSG) {
		VK_MSG = vK_MSG;
	}

	public String getVK_T_DATE() {
		return VK_T_DATE;
	}

	public void setVK_T_DATE(String vK_T_DATE) {
		VK_T_DATE = vK_T_DATE;
	}

	public String getVK_LANG() {
		return VK_LANG;
	}

	public void setVK_LANG(String vK_LANG) {
		VK_LANG = vK_LANG;
	}

	public String toString() {
		return getMessage();
	}

	@Override
	public String getVK_RETURN() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setVK_RETURN(String paramString) {
		// TODO Auto-generated method stub
		
	}
}
