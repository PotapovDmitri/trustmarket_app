package services.payments.messages;

import services.payments.IBanksAuthResponse;

public class IBanksMessage3002 extends IBanksMessage implements IBanksAuthResponse {
	public static final String DEFAULT_SERVICE = "3002";
	protected String VK_USER;
	protected String VK_DATE;
	protected String VK_TIME;
	protected String VK_INFO;

	public IBanksMessage3002() {
		this.VK_SERVICE = "3002";
	}

	public String getMessage() {
		StringBuffer msg = new StringBuffer(fmtElement(this.VK_SERVICE));
		msg.append(fmtElement(this.VK_VERSION));
		msg.append(fmtElement(this.VK_USER));
		msg.append(fmtElement(this.VK_DATE));
		msg.append(fmtElement(this.VK_TIME));
		msg.append(fmtElement(this.VK_SND_ID));
		msg.append(fmtElement(this.VK_INFO));

		return msg.toString();
	}

	public String getVK_USER() {
		return this.VK_USER;
	}

	public void setVK_USER(String VK_USER) {
		this.VK_USER = VK_USER;
	}

	public String getVK_DATE() {
		return this.VK_DATE;
	}

	public void setVK_DATE(String VK_DATE) {
		this.VK_DATE = VK_DATE;
	}

	public String getVK_TIME() {
		return this.VK_TIME;
	}

	public void setVK_TIME(String VK_TIME) {
		this.VK_TIME = VK_TIME;
	}

	public String getVK_INFO() {
		return this.VK_INFO;
	}

	public void setVK_INFO(String VK_INFO) {
		this.VK_INFO = VK_INFO;
	}

	public String toString() {
		return getMessage();
	}
}
