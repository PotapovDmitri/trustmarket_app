package services.payments.messages;

import services.payments.IBanksAuthRequest;
import services.payments.IBanksUtils;


public class IBanksMessage1002 extends IBanksMessage implements
		IBanksAuthRequest {

	
	protected String VK_CURR = "EUR";
	protected String VK_AMOUNT;
	protected String VK_NAME;
	protected String VK_ACC = "";
	protected String VK_REF = "";
	protected String VK_MSG;
	protected String VK_STAMP;
	protected String VK_RETURN;
	protected String VK_CANCEL;
	protected String VK_LANG = "EST";

	public IBanksMessage1002() {
		this.VK_SERVICE = "1002";
	}

	public String getMessage() {
		StringBuffer msg = new StringBuffer(fmtElement(this.VK_SERVICE));
		msg.append(fmtElement(this.VK_VERSION));
		msg.append(fmtElement(this.VK_SND_ID));
		msg.append(fmtElement(this.VK_STAMP));
		msg.append(fmtElement(this.VK_AMOUNT));
		msg.append(fmtElement(this.VK_CURR));
		msg.append(fmtElement(this.VK_ACC));
		msg.append(fmtElement(this.VK_NAME));
		msg.append(fmtElement(this.VK_REF));
		msg.append(fmtElement(this.VK_MSG));

		return msg.toString();
	}

	public String toHTML() {
		StringBuffer buf = new StringBuffer();
		buf.append(super.toHTML());
		buf.append(IBanksUtils.toHTMLHidden("VK_STAMP", getVK_STAMP()));
		buf.append(IBanksUtils.toHTMLHidden("VK_AMOUNT", getVK_AMOUNT()));
		buf.append(IBanksUtils.toHTMLHidden("VK_CURR", getVK_CURR()));
		buf.append(IBanksUtils.toHTMLHidden("VK_ACC", getVK_ACC()));
		buf.append(IBanksUtils.toHTMLHidden("VK_NAME", getVK_NAME()));
		buf.append(IBanksUtils.toHTMLHidden("VK_REF", getVK_REF()));
		buf.append(IBanksUtils.toHTMLHidden("VK_MSG", getVK_MSG()));
		buf.append(IBanksUtils.toHTMLHidden("VK_RETURN", getVK_RETURN()));
		buf.append(IBanksUtils.toHTMLHidden("VK_CANCEL", getVK_CANCEL()));
		buf.append(IBanksUtils.toHTMLHidden("VK_LANG", getVK_LANG()));
		return buf.toString();
	}

	public String getVK_LANG() {
		return VK_LANG;
	}

	public void setVK_LANG(String vK_LANG) {
		VK_LANG = vK_LANG;
	}

	public String getVK_ACC() {
		return VK_ACC;
	}

	public void setVK_ACC(String vK_ACC) {
		VK_ACC = vK_ACC;
	}

	public String getVK_STAMP() {
		return VK_STAMP;
	}

	public void setVK_STAMP(String vK_STAMP) {
		VK_STAMP = vK_STAMP;
	}

	public String getVK_CURR() {
		return VK_CURR;
	}

	public void setVK_CURR(String vK_CURR) {
		VK_CURR = vK_CURR;
	}

	public String getVK_AMOUNT() {
		return VK_AMOUNT;
	}

	public void setVK_AMOUNT(String vK_AMOUNT) {
		VK_AMOUNT = vK_AMOUNT;
	}

	public String getVK_NAME() {
		return VK_NAME;
	}

	public void setVK_NAME(String vK_NAME) {
		VK_NAME = vK_NAME;
	}

	public String getVK_REF() {
		return VK_REF;
	}

	public void setVK_REF(String vK_REF) {
		VK_REF = vK_REF;
	}

	public String getVK_MSG() {
		return VK_MSG;
	}

	public void setVK_MSG(String vK_MSG) {
		VK_MSG = vK_MSG;
	}

	public String getVK_RETURN() {
		return this.VK_RETURN;
	}

	public void setVK_RETURN(String VK_RETURN) {
		this.VK_RETURN = VK_RETURN;
	}

	public String getVK_CANCEL() {
		return this.VK_CANCEL;
	}

	public void setVK_CANCEL(String VK_CANCEL) {
		this.VK_CANCEL = VK_CANCEL;
	}

	public String toString() {
		return getMessage();
	}
}
