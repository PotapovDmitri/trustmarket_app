package services.payments;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.security.AlgorithmParameters;
import java.security.Key;
import java.security.KeyFactory;
import java.security.Signature;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;

import javax.crypto.Cipher;
import javax.crypto.EncryptedPrivateKeyInfo;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import org.apache.commons.codec.binary.Base64;

import services.payments.messages.IBanksMessage;
import sun.misc.BASE64Decoder;
import utils.AppConstants;

public class IBanksService {

	public static final String SYSTEM_ENCODING = Charset.defaultCharset().toString();
	private RSAPrivateKey privKey;
	private RSAPublicKey pubKey;
	

	protected IBanksService(InputStream privKeyStream, InputStream pubKeyStream, String contractID) throws IBanksException {
		try {
			if (privKeyStream != null) {
				//boolean rsaPriv = false;

				byte[] keyBytes = getCertificateBytes(privKeyStream);
				
				
				if ("swedbank".equals(contractID.toLowerCase())) {
					//boolean rsaPriv = PEMInputStream.isPEMRSAPrivateKey(keyBytes);
					ByteArrayInputStream barray = new ByteArrayInputStream(keyBytes);
					PEMInputStream pemIs = new PEMInputStream(barray);
					this.privKey = ((RSAPrivateKey) pemIs.getRSAPrivateKey());
				}else if ("seb".equals(contractID.toLowerCase())) {
					//seb bank
					EncryptedPrivateKeyInfo encryptPKInfo = new EncryptedPrivateKeyInfo(keyBytes);
					Cipher cipher = Cipher.getInstance(encryptPKInfo.getAlgName());
					PBEKeySpec pbeKeySpec = new PBEKeySpec("gggmest6".toCharArray());
					SecretKeyFactory secFac = SecretKeyFactory.getInstance(encryptPKInfo.getAlgName());
					Key pbeKey = secFac.generateSecret(pbeKeySpec);
					AlgorithmParameters algParams = encryptPKInfo.getAlgParameters();
					cipher.init(Cipher.DECRYPT_MODE, pbeKey, algParams);
					KeySpec pkcs8KeySpec = encryptPKInfo.getKeySpec(cipher);
					KeyFactory kf = KeyFactory.getInstance("RSA");
					this.privKey = (RSAPrivateKey) kf.generatePrivate(pkcs8KeySpec);
				}
/*				
				if (PEMInputStream.isPEMSequence(bytes)) {
					rsaPriv = PEMInputStream.isPEMRSAPrivateKey(bytes);
					ByteArrayInputStream barray = new ByteArrayInputStream(bytes);
					PEMInputStream pemIs = new PEMInputStream(barray);
					if (rsaPriv) {
						this.privKey = ((RSAPrivateKey) pemIs.getRSAPrivateKey());
					} else {
						pemIs.decodeAtom();
						bytes = new byte[pemIs.available()];
						pemIs.read(bytes);
					}
				}
				if (!rsaPriv) {
					KeyFactory keyFactory = KeyFactory.getInstance("RSA");
					PKCS8EncodedKeySpec privKeySpec = new PKCS8EncodedKeySpec(bytes);
					this.privKey = ((RSAPrivateKey) keyFactory.generatePrivate(privKeySpec));
				}
*/
				
			}
			
			if (pubKeyStream != null) {
				byte[] bytes = getCertificateBytes(pubKeyStream);

				ByteArrayInputStream barray = new ByteArrayInputStream(bytes);
				if (PEMInputStream.isPEMSequence(bytes)) {
					PEMInputStream pemIs = new PEMInputStream(barray);
					pemIs.decodeAtom();
					bytes = new byte[pemIs.available()];
					pemIs.read(bytes);
					barray = new ByteArrayInputStream(bytes);
				}
				CertificateFactory cf = CertificateFactory.getInstance("X.509");
				X509Certificate cert = (X509Certificate) cf.generateCertificate(barray);
				//cert.checkValidity();
				this.pubKey = ((RSAPublicKey) cert.getPublicKey());

			}
		} catch (Exception e) {
			throw new IBanksException(e.toString());
		}
	}

	private byte[] getCertificateBytes(InputStream keyStream) throws IOException {
		DataInputStream dis = new DataInputStream(new BufferedInputStream(keyStream));
		byte[] bytes = new byte[dis.available()];
		dis.readFully(bytes);
		dis.close();

		return bytes;
	}

/*	private byte[] getCertificate(File file) throws IOException {
		BASE64Decoder b64 = new BASE64Decoder();
		InputStream inStream = new FileInputStream(file);
		byte[] cert = b64.decodeBuffer(inStream);
		inStream.close();
		return cert;
	}*/

	public static IBanksService getInstance(String privKeyFile,
			String pubKeyFile, String contractID) throws IBanksException {
		try {
			FileInputStream privFis = new FileInputStream(new File(privKeyFile));
			FileInputStream pubFis = new FileInputStream(new File(pubKeyFile));

			return new IBanksService(privFis, pubFis, contractID);
		} catch (Exception e) {
			throw new IBanksException(e.toString());
		}
	}

	public static IBanksService getInstance(InputStream privKeyStream,
			InputStream pubKeyStream, String contractID) throws IBanksException {
		return new IBanksService(privKeyStream, pubKeyStream, contractID);
	}

	public final void sign(IBanksMessage message) throws IBanksException {
		try {
			Signature sign = Signature.getInstance("SHA1withRSA");
			sign.initSign(this.privKey);
			sign.update(message.getMessage().getBytes());
			message.setVK_MAC(Base64.encodeBase64String(sign.sign()));
		} catch (Exception e) {
			throw new IBanksException(e.toString());
		}
	}

	public final boolean verify(IBanksMessage message,
			String messageCharsetName, String macCharsetName)
			throws IBanksException {
		boolean result = false;
		try {
			Signature sign = Signature.getInstance("SHA1withRSA");
			sign.initVerify(this.pubKey);

			String msg3002 = message.getMessage();
			byte[] msg;
			if (messageCharsetName == null) {
				msg = msg3002.getBytes();
			} else {
				msg = msg3002.getBytes(messageCharsetName);
			}
			if (macCharsetName != null) {
				String newMac = new String(msg, macCharsetName);
				msg = newMac.getBytes(macCharsetName);
			}
			sign.update(msg);
			result = sign.verify(Base64.decodeBase64(message.getVK_MAC()));

			if (!result) {
				Signature verifier = Signature.getInstance("SHA1withRSA");
				verifier.initVerify(this.pubKey);
				verifier.update(msg3002.getBytes());
				if (verifier.verify(Base64.decodeBase64(message.getVK_MAC()))) {
					result = true;
				} else {
					result = false;
				}

			}

			return result;
		} catch (Exception e) {
			throw new IBanksException(e.toString());
		}
	}

	public final boolean verify(IBanksMessage message) throws IBanksException {
		return verify(message, AppConstants.BANK_DEFAULT_ENCODING, AppConstants.BANK_DEFAULT_ENCODING);
	}

}