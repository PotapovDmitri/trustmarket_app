package services.payments;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.security.KeyException;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.spec.RSAPrivateCrtKeySpec;
import java.util.Vector;

import sun.misc.BASE64Decoder;

public final class PEMInputStream extends FilterInputStream {
	private static final char[] pem_array = { 'A', 'B', 'C', 'D', 'E', 'F',
			'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
			'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
			'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
			't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5',
			'6', '7', '8', '9', '+', '/' };

	private static final byte[] pem_convert_array = new byte[256];
	private InputStream m_is;
	static final int EOF = -1;

	// populate the pem_convert_array
	static {
		for (int i = 0; i < 255; i++)
			pem_convert_array[i] = -1;

		for (int i = 0; i < pem_array.length; i++)
			pem_convert_array[pem_array[i]] = (byte) i;
	}

	public PEMInputStream(InputStream inputstream) {
		super(null);
		this.m_is = inputstream;
	}

	public void decodeAtom() throws IOException {

		if (0 == this.m_is.available()) {
			throw new IOException("PEMInputStream stream is empty.");
		}

		byte decode_buffer[] = new byte[45];
		int i;

		for (i = 0; i < decode_buffer.length; i = decodeAtom(this.m_is,
				decode_buffer, i) != 3 ? 0 : i + 3)
			;

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		baos.write(decode_buffer, 0, i);

		int j = 0;
		while (j <= 3) {

			int k = decodeAtom(this.m_is, decode_buffer, 0);

			if (k > 0) {
				baos.write(decode_buffer, 0, k);

				if (k < 3)
					break;

				j = 0;
				continue;
			}

			if (k == -1)
				break;
			j++;
		}

		super.in = new ByteArrayInputStream(baos.toByteArray());
	}

	// decode one Base64 atom into 1, 2, or 3 bytes of data
	protected int decodeAtom(InputStream input, byte decode_buffer[], int rem)
			throws IOException {
		int j = input.read();

		if (j < 0)
			return -1;

		j = pem_convert_array[j];

		if (j < 0)
			return 0;

		decode_buffer[rem] = (byte) (j << 2);

		j = input.read();

		if (j < 0)
			return -1;

		j = pem_convert_array[j];
		if (j < 0)
			return 0;

		decode_buffer[rem] |= j >> 4;
		decode_buffer[rem + 1] = (byte) (j << 4);

		j = input.read();

		if (j < 0 || j == 61)
			return 1;

		j = pem_convert_array[j];

		if (j < 0)
			return 0;

		decode_buffer[rem + 1] |= j >> 2;
		decode_buffer[rem + 2] = (byte) (j << 6);

		j = input.read();

		if (j < 0 || j == 61)
			return 2;

		j = pem_convert_array[j];

		if (j < 0)
			return 0;
		else {
			decode_buffer[rem + 2] |= j;
			return 3;
		}
	}

	public static boolean isPEMSequence(byte[] keyBytes) {
		boolean res = false;
		if (keyBytes != null) {
			res = new String(keyBytes).indexOf("-----BEGIN") > -1;
		}
		return res;
	}

	public static boolean isPEMRSAPrivateKey(byte[] keyBytes) {
		boolean res = false;
		if (keyBytes != null) {
			res = new String(keyBytes)
					.indexOf("-----BEGIN RSA PRIVATE KEY-----") > -1;
		}
		return res;
	}

	private byte[] parseKey(InputStream is) throws KeyException {
		try {
			BufferedReader fromFile = new BufferedReader(new InputStreamReader(
					is));
			String line = fromFile.readLine();
			String full = "";
			for (; line != null; line = fromFile.readLine()) {
				if (!line.startsWith("-")) {
					full = full + line + "\n";
				}
			}
			fromFile.close();
			return full.getBytes();
		} catch (Exception ex) {
			throw new KeyException("Can not read key data from file", ex);
		}
	}

	public PrivateKey getRSAPrivateKey() throws KeyException {
		PrivateKey res;
		try {
			byte[] keyData = parseKey(this.m_is);
			BASE64Decoder decoder = new BASE64Decoder();
			byte[] decodedKey = decoder.decodeBuffer(new String(keyData));
			AsnInputStream asnIs = new AsnInputStream(decodedKey);
			AsnSequence seq = (AsnSequence) asnIs.read();

			AsnInteger e = (AsnInteger) seq.get(1);
			AsnInteger d = (AsnInteger) seq.get(2);
			AsnInteger p = (AsnInteger) seq.get(3);
			AsnInteger q = (AsnInteger) seq.get(4);
			AsnInteger dmp1 = (AsnInteger) seq.get(5);
			AsnInteger dmq1 = (AsnInteger) seq.get(6);
			AsnInteger u = (AsnInteger) seq.get(7);
			AsnInteger v = (AsnInteger) seq.get(8);
			RSAPrivateCrtKeySpec keySpec = new RSAPrivateCrtKeySpec(
					e.toBigInteger(), d.toBigInteger(), p.toBigInteger(),
					q.toBigInteger(), dmp1.toBigInteger(), dmq1.toBigInteger(),
					u.toBigInteger(), v.toBigInteger());

			KeyFactory kf = KeyFactory.getInstance("RSA");
			res = kf.generatePrivate(keySpec);
		} catch (Exception ex) {
			throw new KeyException("Exception parsing RSA private key.", ex);
		}
		return res;
	}

	private final class AsnInputStream {
		private final InputStream m_is;

		public AsnInputStream(byte[] data) {
			this.m_is = new ByteArrayInputStream(data);
		}

		public AsnInputStream(InputStream is) {
			this.m_is = is;
		}

		public PEMInputStream.AsnObject read() throws IOException {
			int tag = this.m_is.read();
			if (tag == -1) {
				throw new IOException("End of stream.");
			}
			switch (tag) {
			case 2:
				return new PEMInputStream.AsnInteger(this);
			case 48:
				return new PEMInputStream.AsnSequence(this);
			}
			throw new IOException("Unknown ASN object type.");
		}

		public int available() throws IOException {
			return this.m_is.available();
		}

		public int readLength() throws IOException {
			int b = this.m_is.read();
			if (b == -1) {
				throw new IOException("Unexpected end of stream.");
			}
			if (b <= 127) {
				return b;
			}
			b &= 0x7F;
			if (b > 4) {
				throw new IOException("Length too big.");
			}
			int t;
			int res = 0;
			for (res = 0; b-- > 0; res = res << 8 | t) {
				if ((t = this.m_is.read()) == -1) {
					throw new IOException("Unexpected end of stream.");
				}
			}
			if (res < 0) {
				throw new IOException("Negative length.");
			}
			return res;
		}

		byte readByte() throws IOException {
			return readBytes(1)[0];
		}

		byte[] readBytes(int todo) throws IOException {
			byte[] res = new byte[todo];
			int done;
			for (int off = 0; todo > 0; off += done) {
				if ((done = this.m_is.read(res, off, todo)) == -1) {
					throw new IOException("EOF");
				}
				todo -= done;
			}
			return res;
		}

		public AsnInputStream getSubStream(int len) {
			return new AsnInputStream(new PEMInputStream.SubInputStream(this.m_is, len));
		}
	}

	private final class SubInputStream extends InputStream {
		private int m_len;
		private final InputStream m_is;

		public SubInputStream(InputStream is, int len) {
			if (len < 0) {
				throw new IllegalArgumentException("len: < 0");
			}
			this.m_is = is;
			this.m_len = len;
		}

		public int available() throws IOException {
			return this.m_len <= 0 ? 0 : this.m_is.available();
		}

		public int read() throws IOException {
			return this.m_len-- > 0 ? this.m_is.read() : -1;
		}
	}

	private abstract class AsnObject {
		static final byte TAG_MASK = 31;
		static final byte TAG_INTEGER = 2;
		static final byte TAG_BITSTRING = 3;
		static final byte TAG_NULL = 5;
		static final byte TAG_OBJECT_ID = 6;
		static final byte TAG_SEQUENCE = 48;
		private final byte m_tag;

		protected AsnObject(byte tag) {
			this.m_tag = tag;
		}
	}

	private final class AsnInteger extends PEMInputStream.AsnObject {
		private final BigInteger m_val;

		private AsnInteger(PEMInputStream.AsnInputStream is) throws IOException {
			super((byte) 2);
			int len = is.readLength();
			byte[] data = is.readBytes(len);
			this.m_val = new BigInteger(data);
		}

		public AsnInteger(BigInteger value) {
			super((byte) 2);
			this.m_val = value;
		}

		public BigInteger toBigInteger() {
			return this.m_val;
		}
	}

	private final class AsnSequence extends PEMInputStream.AsnObject {
		private final PEMInputStream.AsnObject[] m_vals;

		private AsnSequence(PEMInputStream.AsnInputStream is)
				throws IOException {
			super((byte) 48);
			int len = is.readLength();
			PEMInputStream.AsnInputStream sub_is = is.getSubStream(len);
			Vector<PEMInputStream.AsnObject> vec = new Vector(3);
			while (sub_is.available() > 0) {
				vec.addElement(sub_is.read());
			}
			vec.copyInto(this.m_vals = new PEMInputStream.AsnObject[vec.size()]);
		}

		public AsnSequence(PEMInputStream.AsnObject[] vals) {
			super((byte) 48);
			this.m_vals = ((PEMInputStream.AsnObject[]) vals.clone());
		}

		public PEMInputStream.AsnObject get(int index) {
			return this.m_vals[index];
		}

		public int size() {
			return this.m_vals.length;
		}
	}
}
