package services.payments;

public class IBanksException extends Exception {

	public IBanksException(String str) {
		super(str);
	}

	public IBanksException() {
	}

}
