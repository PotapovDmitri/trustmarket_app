package services.payments;

public abstract interface IBanksAuthResponse extends IBanksResponse {

	public abstract String getVK_INFO();

	public abstract void setVK_INFO(String paramString);

}
