package services.payments;

import java.util.HashMap;
import java.util.StringTokenizer;

import services.payments.messages.IBanksMessage;

public abstract class IBanksUtils
{
  static char[] hexChar = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
  
  public static String toHTMLHidden(String name, String value)
  {
    StringBuffer buf = new StringBuffer();
    buf.append("<input type=\"hidden\" name=\"");
    buf.append(name);
    buf.append("\" value=\"");
    buf.append(value);
    buf.append("\">\n");
    return buf.toString();
  }
  
  public static String messageToHTMLForm(ConfigurableIBanksService client, IBanksMessage msg, boolean autoSubmit)
    throws IBanksException
  {
    if ((msg instanceof IBanksAuthRequest))
    {
      StringBuffer buf = new StringBuffer();
      
      buf.append("<html>\n");
      buf.append("<body>\n");
      buf.append("<form method=\"post\" target=\"_self\" action=\"");
      buf.append(client.getActionUrl());
      buf.append("\">\n");
      
      buf.append(msg.toHTML());
      if (autoSubmit)
      {
        buf.append("<script>");
        buf.append("document.forms[0].submit();");
        buf.append("</script>\n");
      }
      else
      {
        buf.append("<input type=\"submit\" value=\"Suuna IPizza autentimisele ").append(client.getContractID()).append(" \">\n");
      }
      buf.append("</form>\n");
      buf.append("</body>\n");
      buf.append("</html>\n");
      return buf.toString();
    }
    throw new IBanksException("IPizza message must be an instance of IPizzaAuthRequest.");
  }
  
  public static HashMap parseInfoData(String vkInfo)
  {
    HashMap data = new HashMap(0);
    if (vkInfo != null)
    {
      StringTokenizer st = new StringTokenizer(vkInfo, ";");
      while (st.hasMoreElements())
      {
        StringTokenizer dt = new StringTokenizer(st.nextToken(), ":");
        
        String key = dt.nextToken().trim();
        if (dt.hasMoreElements())
        {
          String value = dt.nextToken().trim();
          data.put(key, value);
        }
      }
    }
    return data;
  }
  
  public static String getInfoField(String vkInfo, String key)
  {
    return (String)parseInfoData(vkInfo).get(key);
  }
  
  public static String toHexString(byte[] bytes)
  {
    StringBuffer sb = new StringBuffer(bytes.length * 2);
    for (byte aB : bytes)
    {
      sb.append(hexChar[((aB & 0xF0) >>> 4)]);
      

      sb.append(hexChar[(aB & 0xF)]);
    }
    return sb.toString();
  }
  
  private static int charToNibble(char c)
  {
    if (('0' <= c) && (c <= '9')) {
      return c - '0';
    }
    if (('a' <= c) && (c <= 'f')) {
      return c - 'a' + 10;
    }
    if (('A' <= c) && (c <= 'F')) {
      return c - 'A' + 10;
    }
    throw new IllegalArgumentException("Vigane HEX tähemärk: " + c);
  }
  
  public static byte[] fromHexString(String text)
  {
    int stringLength = text.length();
    if ((stringLength & 0x1) != 0) {
      throw new IllegalArgumentException("Sisend peab olema paarisarv HEX tähemärke.");
    }
    byte[] b = new byte[stringLength / 2];
    
    int i = 0;
    for (int j = 0; i < stringLength; j++)
    {
      int high = charToNibble(text.charAt(i));
      int low = charToNibble(text.charAt(i + 1));
      b[j] = ((byte)(high << 4 | low));i += 2;
    }
    return b;
  }
}
