package services.payments;

public abstract interface IBanksRequest {
  public abstract String getVK_RETURN();
  
  public abstract void setVK_RETURN(String paramString);
}
