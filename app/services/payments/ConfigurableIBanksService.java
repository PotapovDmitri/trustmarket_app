package services.payments;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import play.Play;

public class ConfigurableIBanksService extends IBanksService {

	public static final String DEFAULT_PROPERTIES_FILE = "banks.properties";
	private String vkRecId;
	private String actionUrl;
/*
	private String returnUrl1001; 
	private String cancelUrl1001;
	private String returnUrl4001;
*/
	private String returnUrl1111;
	private String cancelUrl1911;
	private String contractID;

	private ConfigurableIBanksService(InputStream privKeyStream,
			InputStream pubKeyStream, String contractID) throws IBanksException {
		super(privKeyStream, pubKeyStream, contractID);
	}

	public static ConfigurableIBanksService getInstance(String contractID)
			throws IBanksException {
		try {

			Properties props = new Properties();
			props.load(Play.application().resourceAsStream(DEFAULT_PROPERTIES_FILE));

			return initializeInstance(contractID, props);
		} catch (IOException e) {
			throw new IBanksException("Not found '" + DEFAULT_PROPERTIES_FILE + "'");
		}
	}

	public static ConfigurableIBanksService getInstance(String contractID,
			Properties props) throws IBanksException {
		return initializeInstance(contractID, props);
	}

	private static ConfigurableIBanksService initializeInstance(
			String contractID, Properties props) throws IBanksException {

		ConfigurableIBanksService service = null;

		String privKey = props.getProperty(contractID + ".PRIVKEY");
		String pubKey = props.getProperty(contractID + ".CERTIFICATE");
		if ((privKey != null) && (pubKey != null)) {
			
			service = new ConfigurableIBanksService(Play.application().resourceAsStream(privKey), Play.application().resourceAsStream(pubKey), contractID);
			service.setContractID(contractID);
			service.setVkRecId(props.getProperty(contractID + ".VK_REC_ID"));
			service.setActionUrl(props.getProperty(contractID + ".ACTION_URL"));
			/*
			service.setReturnUrl1001(props.getProperty(contractID + ".1001_RETURN_URL"));
			service.setCancelUrl1001(props.getProperty(contractID + ".1001_CANCEL_URL"));
			service.setReturnUrl4001(props.getProperty(contractID + ".4001_RETURN_URL"));
			*/

			service.setReturnUrl1111(props.getProperty(contractID + ".1111_RETURN_URL"));
			service.setCancelUrl1911(props.getProperty(contractID + ".1911_CANCEL_URL"));
			
		} else {
			throw new IBanksException("Not found '" + contractID + "' configuration.");
		}
		return service;
	}

	public String getVkRecId() {
		return vkRecId;
	}

	public void setVkRecId(String vkRecId) {
		this.vkRecId = vkRecId;
	}

	public String getActionUrl() {
		return actionUrl;
	}

	public void setActionUrl(String actionUrl) {
		this.actionUrl = actionUrl;
	}
/*
	public String getReturnUrl1001() {
		return returnUrl1001;
	}

	public void setReturnUrl1001(String returnUrl1001) {
		this.returnUrl1001 = returnUrl1001;
	}

	public String getCancelUrl1001() {
		return cancelUrl1001;
	}

	public void setCancelUrl1001(String cancelUrl1001) {
		this.cancelUrl1001 = cancelUrl1001;
	}

	public String getReturnUrl4001() {
		return returnUrl4001;
	}

	public void setReturnUrl4001(String returnUrl4001) {
		this.returnUrl4001 = returnUrl4001;
	}
*/
	
	public String getContractID() {
		return contractID;
	}

	public void setContractID(String contractID) {
		this.contractID = contractID;
	}

	public String getReturnUrl1111() {
		return returnUrl1111;
	}

	public void setReturnUrl1111(String returnUrl1111) {
		this.returnUrl1111 = returnUrl1111;
	}

	public String getCancelUrl1911() {
		return cancelUrl1911;
	}

	public void setCancelUrl1911(String cancelUrl1911) {
		this.cancelUrl1911 = cancelUrl1911;
	}

	/*
	public String getReturnUrl1101() {
		return returnUrl1101;
	}

	public void setReturnUrl1101(String returnUrl1101) {
		this.returnUrl1101 = returnUrl1101;
	}

	public String getCancelUrl1901() {
		return cancelUrl1901;
	}

	public void setCancelUrl1901(String cancelUrl1901) {
		this.cancelUrl1901 = cancelUrl1901;
	}
	*/
	
	
	
}