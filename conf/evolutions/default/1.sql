# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table admin_stores (
  store_id                  bigint not null,
  name_ru                   varchar(255),
  name_est                  varchar(255),
  name_eng                  varchar(255),
  desc_ru                   varchar(255),
  desc_est                  varchar(255),
  desc_eng                  varchar(255),
  image                     varchar(255),
  link                      varchar(255),
  store_view                boolean,
  constraint pk_admin_stores primary key (store_id))
;

create table bills (
  bill_id                   bigint not null,
  total_summa               integer,
  product_summa             integer,
  service_summa             integer,
  delivery_summa            integer,
  created                   timestamp,
  action_date               timestamp,
  payed                     timestamp,
  unpayed                   timestamp,
  bill_state                varchar(255),
  user_request_id           bigint,
  payment_method            varchar(255),
  constraint pk_bills primary key (bill_id))
;

create table request (
  request_id                bigint not null,
  type                      varchar(255),
  store_link                varchar(255),
  name                      varchar(255),
  size                      varchar(255),
  article                   varchar(255),
  package_code              varchar(255),
  price                     integer,
  total_summa               integer,
  count                     integer,
  color                     varchar(255),
  description               varchar(255),
  created                   timestamp,
  user_request_id           bigint,
  constraint pk_request primary key (request_id))
;

create table request_messages (
  request_message_id        bigint not null,
  message                   varchar(255),
  request_message_type      varchar(255),
  user_request_id           bigint,
  user_id                   bigint,
  reply_message_id          bigint,
  created                   timestamp,
  constraint pk_request_messages primary key (request_message_id))
;

create table roles (
  role_id                   bigint not null,
  role_name                 varchar(255),
  constraint pk_roles primary key (role_id))
;

create table users (
  user_id                   bigint not null,
  username                  varchar(255),
  password                  VARCHAR(255),
  first_name                varchar(255),
  last_name                 varchar(255),
  birthday                  timestamp,
  passport_nr               varchar(255),
  email                     varchar(255),
  created                   timestamp,
  role_id                   bigint,
  constraint pk_users primary key (user_id))
;

create table user_requests (
  user_request_id           bigint not null,
  delivery_price            integer,
  status                    varchar(255),
  created                   timestamp,
  sended                    timestamp,
  user_id                   bigint,
  constraint pk_user_requests primary key (user_request_id))
;

create sequence admin_stores_seq;

create sequence bills_seq;

create sequence request_seq;

create sequence request_messages_seq;

create sequence roles_seq;

create sequence users_seq;

create sequence user_requests_seq;

alter table bills add constraint fk_bills_userRequests_1 foreign key (user_request_id) references user_requests (user_request_id);
create index ix_bills_userRequests_1 on bills (user_request_id);
alter table request add constraint fk_request_userRequests_2 foreign key (user_request_id) references user_requests (user_request_id);
create index ix_request_userRequests_2 on request (user_request_id);
alter table request_messages add constraint fk_request_messages_userReques_3 foreign key (user_request_id) references user_requests (user_request_id);
create index ix_request_messages_userReques_3 on request_messages (user_request_id);
alter table request_messages add constraint fk_request_messages_user_4 foreign key (user_id) references users (user_id);
create index ix_request_messages_user_4 on request_messages (user_id);
alter table request_messages add constraint fk_request_messages_replyMessa_5 foreign key (reply_message_id) references request_messages (request_message_id);
create index ix_request_messages_replyMessa_5 on request_messages (reply_message_id);
alter table users add constraint fk_users_role_6 foreign key (role_id) references roles (role_id);
create index ix_users_role_6 on users (role_id);
alter table user_requests add constraint fk_user_requests_user_7 foreign key (user_id) references users (user_id);
create index ix_user_requests_user_7 on user_requests (user_id);



# --- !Downs

drop table if exists admin_stores cascade;

drop table if exists bills cascade;

drop table if exists request cascade;

drop table if exists request_messages cascade;

drop table if exists roles cascade;

drop table if exists users cascade;

drop table if exists user_requests cascade;

drop sequence if exists admin_stores_seq;

drop sequence if exists bills_seq;

drop sequence if exists request_seq;

drop sequence if exists request_messages_seq;

drop sequence if exists roles_seq;

drop sequence if exists users_seq;

drop sequence if exists user_requests_seq;

